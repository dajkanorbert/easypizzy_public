<h3>A kosár tartalma</h3>
<hr>
<?php

	$vid = $_SESSION['vid'];

	// ha rákattintott a megrendel gombra
	if (isset($_POST['megrendel']))
	{
		$szallmod = $_POST['szallitas'];
		$fizmod = $_POST['fizetes'];
		$vegosszeg = $_POST['vegosszeg'];

		if (isset($_POST['kedvezmeny'])) {
			$vegosszeg-=$_POST['kedvezmenyosszeg'];
		}

		$cim = $_POST['cimek'];
		$megjegyzes = escapeshellcmd($_POST['megjegyzes']);

		if (empty($szallmod) || empty($fizmod))
		{
			showError("Add meg a szállítási és fizetési információkat!");
		}
		else
		{
			$vasID = $_SESSION['vid'];
			// beírjuk a rendelés táblába
			$db->query("INSERT INTO rendelesek VALUES(null, CURRENT_TIMESTAMP, $vasID, null, null, null, 0, $vegosszeg, 1, '$fizmod', '$szallmod', null,'$megjegyzes','$cim')");
			$rendelesID = $db->lastID();


			if (isset($_POST['kedvezmeny'])) {
				$db->query("UPDATE vasarlok SET penztarca=0 WHERE ID=$vasID");
			}


			$db->query("UPDATE vasarlok SET vasalk=vasalk+1 , vasosszeg=vasosszeg+'$vegosszeg' WHERE ID=$vasID");


			
			// átírjuk a tételeket a kosárból a rendelestetel táblába
			$db->query("SELECT * FROM kosar WHERE kategoria='Pizza' AND vasarloID=$vasID");
			$tetelek = $db->fetchAll();

			foreach ($tetelek as $tetel) 
			{
				$db->query("INSERT INTO rendelestetel VALUES(
					null, 
					".$rendelesID.",
					".$vid.",
					".$tetel['termekID'].",
					47,
					27,
					".$tetel['mennyiseg'].",
					0,
					'".$fizmod."',
					'".$szallmod."',
					'".$cim."',
					'".$megjegyzes."',
					".$vegosszeg.",
					1,
					0
				)");
			}
			// kitoroljük a felhasználó kosarát
			$db->query("DELETE FROM kosar WHERE kategoria='Pizza' AND vasarloID=$vasID");








			$db->query("SELECT * FROM kosar WHERE kategoria='Üdítő' AND vasarloID=$vasID");
			$tetelek = $db->fetchAll();

			foreach ($tetelek as $tetel) 
			{
				$db->query("INSERT INTO rendelestetel VALUES(
					null, 
					".$rendelesID.",
					".$vid.",
					27,
					".$tetel['termekID'].",
					27,
					".$tetel['mennyiseg'].",
					0,
					'".$fizmod."',
					'".$szallmod."',
					'".$cim."',
					'".$megjegyzes."',
					".$vegosszeg.",
					1,
					0
				)");
			}
			// kitoroljük a felhasználó kosarát
			$db->query("DELETE FROM kosar WHERE kategoria='Üdítő' AND vasarloID=$vasID");
		


			
			$db->query("SELECT * FROM kosar WHERE kategoria='Desszert' AND vasarloID=$vasID");
			$tetelek = $db->fetchAll();

			foreach ($tetelek as $tetel) 
			{
				$db->query("INSERT INTO rendelestetel VALUES(
					null, 
					".$rendelesID.",
					".$vid.",
					27,
					47,
					".$tetel['termekID'].",	
					".$tetel['mennyiseg'].",
					0,
					'".$fizmod."',
					'".$szallmod."',
					'".$cim."',
					'".$megjegyzes."',
					".$vegosszeg.",
					1,
					0
				)");
			}


			// kitoroljük a felhasználó kosarát
			$db->query("DELETE FROM kosar WHERE kategoria='Desszert' AND vasarloID=$vasID");
		
			if (isset($_POST['kedvezmeny'])) {
				$db->query("INSERT INTO rendelestetel VALUES(
					null, 
					".$rendelesID.",
					".$vid.",
					27,
					47,
					27,	
					1,
					0,
					'".$fizmod."',
					'".$szallmod."',
					'".$cim."',
					'kedvezmény',
					".($_POST['kedvezmenyosszeg']*(-1)).",
					1,
					0
				)");
			}

			


			showSuccess("A rendelését rögzítettük!");
			// a vásárló statisztikáját frissítsük -> a rendelés lezárásakor kell
			/*
			$vegosszeg = $_POST['vegosszeg'];
			$db->query("UPDATE vasarlok SET 
				vas_alk = vas_alk + 1, 
				vas_ossz = vas_ossz + $vegosszeg
				WHERE ID=$vasID");
			

			*/
		}
	}
	$fizetendo = 0;
	echo '<h3>Pizza</h3>';
	$db->query("SELECT 
		kosar.ID AS '@ID',
		attekinto.megnevezes AS 'Terméknév',
		kosar.mennyiseg AS 'Mennyiség',
		attekinto.ar AS 'Egys.ár',
		attekinto.ar * kosar.mennyiseg AS 'Össz.ár'
	 FROM kosar 
	 INNER JOIN pizzak ON pizzak.ID = kosar.termekID
	 INNER JOIN attekinto ON attekinto.ID = pizzak.ID
	 WHERE kosar.kategoria='Pizza' AND kosar.vasarloID=".$_SESSION['vid']);
	$db->convertTable('d');

	$db->query("SELECT 
		
		SUM(attekinto.ar * kosar.mennyiseg) AS 'Fizetendő'
	 FROM kosar 
	 INNER JOIN pizzak ON pizzak.ID = kosar.termekID
	 INNER JOIN attekinto ON attekinto.ID = pizzak.ID
	 WHERE kosar.kategoria='Pizza' AND kosar.vasarloID=".$_SESSION['vid']);

	$res = $db->fetchAll();
	$fizetendo += $res[0]['Fizetendő'];

	echo '<h3>Üdítő</h3>';
	$db->query("SELECT 
		kosar.ID AS '@ID',
		uditok.megnevezes AS 'Terméknév',
		kosar.mennyiseg AS 'Mennyiség',
		uditok.ar AS 'Egys.ár',
		uditok.ar * kosar.mennyiseg AS 'Össz.ár'
	 FROM kosar 
	 INNER JOIN uditok ON uditok.ID = kosar.termekID
	 WHERE kosar.kategoria='Üditő' AND kosar.vasarloID=".$_SESSION['vid']);
	$db->convertTable('d');

	$db->query("SELECT 
		
		SUM(uditok.ar * kosar.mennyiseg) AS 'Fizetendő'
	 FROM kosar 
	 INNER JOIN uditok ON uditok.ID = kosar.termekID
	 WHERE kosar.kategoria='Üdítő' AND kosar.vasarloID=".$_SESSION['vid']);

	$res = $db->fetchAll();
	$fizetendo += $res[0]['Fizetendő'];


	echo '<h3>Desszert</h3>';
	$db->query("SELECT 
		kosar.ID AS '@ID',
		desszertek.megnevezes AS 'Terméknév',
		kosar.mennyiseg AS 'Mennyiség',
		desszertek.ar AS 'Egys.ár',
		desszertek.ar * kosar.mennyiseg AS 'Össz.ár'
	 FROM kosar 
	 INNER JOIN desszertek ON desszertek.ID = kosar.termekID
	 WHERE kosar.kategoria='Desszert' AND kosar.vasarloID=".$_SESSION['vid']);
	$db->convertTable('d');

	$db->query("SELECT 
		
		SUM(desszertek.ar * kosar.mennyiseg) AS 'Fizetendő'
	 FROM kosar 
	 INNER JOIN desszertek ON desszertek.ID = kosar.termekID
	 WHERE kosar.kategoria='Desszert' AND kosar.vasarloID=".$_SESSION['vid']);

	$res = $db->fetchAll();
	$fizetendo += $res[0]['Fizetendő'];

	$db->query("SELECT penztarca FROM vasarlok WHERE ID=$vid");
	$pont = $db->fetchAll();

	print '<br> Felhasználható EasyPizzy krediteim: <span class="green">'.($pont[0]['penztarca']. ' </span>'.$penznem.' értékben!');



	echo '<br>A fizetendő teljes összeg: <span class="redosszeg"> <b>'.szamkiir($fizetendo).'</b></span> '.$penznem.'
	<br><br>


	<form method="POST" action="index.php?pg=kosarinfo">
	<div class="form-group">
	<input type="checkbox" name="kedvezmeny">Felhasználom a kedvezményem!
	<input type="hidden" name="kedvezmenyosszeg" value="'.$pont[0]['penztarca'].'">
	</div>


	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">


		<div class="form-group">
			<label>Fizetési mód:</label>
			<select name="fizetes" class="form-control">
				<option value="">Válasszon...</option>
				<option value="Készpénz">Futárnál fizetek készpénzzel</option>
				<option value="Bankkártya">Futárnál fizetek bankkártyával</option>
				<option value="PayPal">PayPal</option>
			</select>
		</div>
		<div class="form-group"> 
			<label>Szállítási cím:</label><br>
			<a href="index.php?pg=szemelyes/cimek"><i class="far fa-plus-square"></i> Új szállítási cím felvétele</a>
			';
			$db->query("SELECT cim FROM cimek WHERE vasarloID=$vid");
			$db->convertSelect('cim','cim','');
		echo'</div>

		<div class="form-group">
			<label>Szállítási mód:</label>
			<select name="szallitas" class="form-control">
				<option value="">Válasszon...</option>
				<option value="Érte megyek">Érte megyek</option>
				<option value="Házhozszállítás">Házhozszállítás</option>
			</select>
		</div>

		<div class="form-group">
			<label>Megjegyzés:</label>
			<textarea name="megjegyzes" class="form-control"></textarea>
		</div>	
		<div class="form-group">
			<input type="hidden" name="vegosszeg" value="'.$fizetendo.'">
			<input type="submit" name="megrendel" value="Rendelés leadása" class="btn btn-danger">
		</div>	
	</form>
	</div>
	<div class="col-xs-12 col-sm-3"></div>';
?>
