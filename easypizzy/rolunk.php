<h3>Rólunk</h3>
<hr>


<p class="j">
	

Napjainkban hatalmas teret hódít a házhozszállítás. Jelenleg a csomag és az étel az, ami elsősorban eszünkbe jut a „házhozszállítás” szóról.</p>

<p class="j"> Külföldön és hazánkban, nagyobb városokban már van lehetőség hipermarketekből is rendelni és csak kiszállításkor, az üzletlánc alkalmazottjánál egyenlíteni a számlát. Saját véleményem szerint az időhiány és a kényelem az oka így még nagyobb teret fog nyerni és pár éven belül rendkívül felkapott lesz, hogy az internet segítségével, pár kattintással leadott termék, szolgáltatásigényünk „pár perc” múlva a bejárati ajtónkon kopogtasson.</p>

 <p class="j">Ahogy halad a technológia előre, ez a logisztikai szakág is robotosítva lesz és egyre több drón fog körülöttünk repkedni, akik épp szállítják a vasárnapi húsleveshez a szombati délutáni bevásárlás közepette elfelejtett kéttojásos cérnametéltet. A vendéglátás úgy gondolom sokat fejlődött etéren és hatalmas divat lett az étel házhozszállítás főleg, hogy a kínálat ma már megszámlálhatatlan. Többféle diétába illeszthető ételek, ételintoleranciában szenvedők is megtalálhatják a saját ízlésüknek megfelelő fogásokat. Az éttermekben, bisztrókban egy finom vacsora nem csak anyagilag megterhelő, de több órát is rá kell szánni mind az étel elkészülésének várakozására, mind a rendelésünk kényelmes elfogyasztására, nem beszélve az ezt megelőző otthoni készülődésre, hogy megjelenésünk ne hagyjon kívánnivalót maga után. Ezzel szemben a házhozszállításnak hatalmas előnye, hogy amíg az ételt várjuk hasznosan, vagy akár szórakozva tölthetjük az időt. </p>

 <p class="j">Külön weboldalak készülnek már az egyes éttermek kínálataink összehasonlítására. Csábító kuponok szólítják meg a potenciális vendéget, hogy éljen a haza rendelésszolgáltatással, hiszen így a vendéglátóegységnek is „több széke marad” további vendégek kiszolgálására. A minőség szigorúan ugyanaz mintha helyben fogyasztanánk, talán a tálalás hagy némi kívánnivalót maga után, hiszen egyszer használatos éthordóba mégse tudja olyan szinten kiélni magát a szakember. Már csak azért is ragaszkodnak a minőséghez, hiszen a rendelés után a vendég email címére kap egy formaüzenetet melyben az étel minőségére, a futár iránt érzett szimpátiájának értékelésére kérik. Amennyiben a vendég nem volt maradéktalanul elégedett az esetek túlnyomó többségében hangot is ad véleményének és pár kattintással hatalmasat tud csorbítani az őt kiszolgáló étterem online értékelésében, megítélésében.</p>

	<a class="black" href="index.php?pg=easypizzy">Vissza az infókhoz</a>