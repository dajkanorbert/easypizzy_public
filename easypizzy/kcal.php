<h3>Kalória Infó</h3>
<hr>
A kalória az energiának a mértékegysége: egy kalória egy gramm víz hőmérsékletét egy Celsius-fokkal képes megemelni. A mértékegységet 1824-ben vezette be Nicolas Clément. Ma már – mióta bevezették az SI mértékegységet – jórészt a joule-t használjuk a kalória helyett. Manapság a kalóriát az egyes élelmiszere energiatartalmának mérésére használják.
<h2>Kalória a táplálkozásban</h2>
Egy kalória megközelítőleg 4,2 joule. (Ez azonban azért csak közelítő érték, mivel a víz fűtéséhez szükséges energiamennyiséget a nyomás és a kezdeti hőmérséklet is befolyásolja, így a pontos érték általában 4,18 és 4,205 joule között mozog. Az is problémát okoz, hogy a köznyelvben a kilokalóriát is kalóriának nevezik.)

A definíciók különbségéből adódóan többféle kalóriát használunk: ezek közül a legelterjedtebb a 15 Celsius-fokos kalória, a terokémikus kalória (ez pontosan 4,184 joule). Ezenkívül használják még az energetikában az IT kalóriát (International Table calorie, 4,2868 joule). A különbségek abból adódnak, hogy a kalória és a joule egymás közötti átszámítása egymástól eltérő adatokkal történt.

Az ember kizárólag a táplálékból (fehérje, zsír, szénhidrát) képes energiát nyerni. A zsírok átlagosan 9,3 kalóriát tartalmaznak grammonként, a fehérjék ennél kevesebbet, 4,2 kalóriát grammonként és a szénhidrátokkal grammonként 4,1 kalória energiát nyerhetünk.
<h2>A mértékegység működése, használata</h2>
A mértékegységet sajnos hiányosan használják: a tápanyagok energiatartalmát KJ/kg-ban kellene megadni, de általában csak 100 grammal számolnak.

A kalória mértékegységét elavultnak minősítették a 8/1976 (IV. 27.) Minisztertanácsi rendelet szerint, mindössze még 4 évre engedélyezte a használatát. A NIST jelenleg is tiltott mértékegységként tartja számon.

Az egyes táplálékok, élelmiszerek csomagolásán még ma is találkozunk a kalóriával, hiszen a táplálkozástudományban használatos táblázatok még kalóriában készültek. Jelenleg azt tapasztaljuk, hogy egy termék tápértékét kalóriában és kilojoule-ban is feltüntetik.

E blog célja, hogy bemutasson néhány olyan módszert, mellyel csökkenthető a kalóriabevitelünk: beszélni fogunk a kókuszolajról, a nyírfacukorról, a jázminpakócáról és az édesgyökérről is. <br> <br>

<i>Forrás: kaloriatablazat.net</i> <br>

	<a class="black" href="index.php?pg=easypizzy">Vissza az infókhoz</a>