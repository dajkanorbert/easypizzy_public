<h3>Tétel törlése</h3>
<hr>
<?php

	//a törlendő termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintott a töröl gombra
	if (isset($_POST['torol']))
	{
		//kitöröljük az adatbázisból és visszairányítjuk a kosárba
		$db->query("DELETE FROM kosar WHERE ID=$id");
		header("location: index.php?pg=kosarinfo");
	}

	// ide fut először, hiszen az oldal betöltése után nem kattintottunk egyből a végleges törlés gombra, biztonsági kérdés a termék törléséről
	echo 'Biztosan törlöd a tételt?';

	echo '<form method="POST" action="index.php?pg=kosar_del&id='.$id.'">
		<div class="form-group">
			<input type="submit" name="torol" value="Igen" class="btn btn-danger">
			<a href="?pg=kosarinfo" class="btn btn-primary">Mégsem</a>
		</div>
	</form>';
?>
