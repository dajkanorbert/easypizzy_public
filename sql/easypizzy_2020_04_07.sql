-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2020. Ápr 07. 21:05
-- Kiszolgáló verziója: 10.4.11-MariaDB
-- PHP verzió: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `zd_dajkan`
--

-- --------------------------------------------------------

--
-- A nézet helyettes szerkezete `attekinto`
-- (Lásd alább az aktuális nézetet)
--
CREATE TABLE `attekinto` (
`ID` int(11)
,`megnevezes` varchar(100)
,`meret` varchar(100)
,`teszta` varchar(100)
,`szosz` varchar(100)
,`szoszleiras` varchar(100)
,`hus` varchar(100)
,`husleiras` varchar(100)
,`zoldseg` varchar(100)
,`zoldsegleiras` varchar(100)
,`sajt` varchar(100)
,`sajtleiras` varchar(100)
,`ar` bigint(25)
,`kcal` bigint(15)
,`kep` int(100)
);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `cimek`
--

CREATE TABLE `cimek` (
  `ID` int(11) NOT NULL,
  `vasarloID` int(11) NOT NULL,
  `utca` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `hsz` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `kt` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `megjegyzes` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `cimek`
--

INSERT INTO `cimek` (`ID`, `vasarloID`, `utca`, `hsz`, `kt`, `megjegyzes`) VALUES
(2, 5, 'Rákóczi u.', '34', 'kaputelefon:5', ''),
(3, 8, 'Széchenyi', '55', 'kt.:16 3/4', ''),
(5, 4, 'Jókai', '18', '10', 'otthoni ^#1'),
(6, 4, 'Cinege', '10', '-', 'otthoni');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `desszertek`
--

CREATE TABLE `desszertek` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `kcal` int(11) DEFAULT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `desszertek`
--

INSERT INTO `desszertek` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Somlói Galuska', 'Piskótatésztából, csokoládéöntettel és tejszínhab díszítéssel', 197, 1590),
(2, 'Mogyorós Fánk', 'Mogyoróreszelékkel megszórt kör alakú fánk', 432, 390),
(3, 'Nutellás palacsinta', 'Nutellával megkent palacsinta (2db)', 710, 790),
(4, 'Áfonyás Muffin', 'Áfonyás Muffin', 347, 390);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `felhasznalok`
--

CREATE TABLE `felhasznalok` (
  `ID` int(11) NOT NULL,
  `nev` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `jelszo` varchar(32) COLLATE utf8_hungarian_ci NOT NULL,
  `tel` varchar(20) COLLATE utf8_hungarian_ci NOT NULL,
  `beosztas` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(11) NOT NULL,
  `pontszam` int(11) NOT NULL,
  `profil` int(11) NOT NULL,
  `avatar` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `regdatum` datetime NOT NULL,
  `utbelepdatum` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `felhasznalok`
--

INSERT INTO `felhasznalok` (`ID`, `nev`, `email`, `jelszo`, `tel`, `beosztas`, `statusz`, `pontszam`, `profil`, `avatar`, `regdatum`, `utbelepdatum`) VALUES
(2, 'Futár', 'futar@easypizzy.hu', '21232f297a57a5a743894a0e4a801fc3', '+3612345678', 'Futár', 4, 0, 0, '<img src=\"images/avatar/nopic.jpg\">', '2020-02-14 00:00:00', '2020-02-23 13:58:31'),
(3, 'Futár2', 'futar2@easypizzy.hu', '21232f297a57a5a743894a0e4a801fc3', '+3612345678', 'Futár', 1, 0, 0, '', '2020-02-14 00:00:00', '2020-02-14 21:04:00'),
(4, 'Szakács', 'szakacs@easypizzy.hu', '21232f297a57a5a743894a0e4a801fc3', '+3612345678', 'Szakács', 1, 0, 0, '', '2020-02-14 00:00:00', '2020-02-14 00:00:00'),
(5, 'Szakács2', 'szakacs2@easypizzy.hu', '21232f297a57a5a743894a0e4a801fc3', '+3612345678', 'Szakács', 1, 0, 0, '', '2020-02-14 00:00:00', '2020-02-14 00:00:00'),
(6, 'Üzletvezető', 'uzletvezeto@easypizzy.hu', '21232f297a57a5a743894a0e4a801fc3', '+3654456585', 'Üzletvezető', 1, 0, 0, '', '2020-02-14 00:00:00', '2020-02-14 00:00:00'),
(7, 'ADMIN', 'admin', '21232f297a57a5a743894a0e4a801fc3', '+36', 'ADMIN', 3, 0, 0, '0', '2020-02-22 00:00:00', '2020-04-07 20:28:47');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `fizmodok`
--

CREATE TABLE `fizmodok` (
  `ID` int(11) NOT NULL,
  `fizmod` varchar(100) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `fizmodok`
--

INSERT INTO `fizmodok` (`ID`, `fizmod`) VALUES
(1, 'Készpénz'),
(2, 'SZÉP kártya'),
(3, 'Bankkártya');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek`
--

CREATE TABLE `hirek` (
  `ID` int(11) NOT NULL,
  `felhasznaloID` int(11) NOT NULL,
  `hir` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `datum` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `hirek`
--

INSERT INTO `hirek` (`ID`, `felhasznaloID`, `hir`, `datum`) VALUES
(1, 7, 'Teszt hír', '2020-04-07 17:00:00');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `husok`
--

CREATE TABLE `husok` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `kcal` int(11) DEFAULT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `husok`
--

INSERT INTO `husok` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Sonka', 'Pizzasonka lágy szeletekre feldolgozva', 155, 200),
(2, 'Csirkemell', 'Elősütött, fűszerezett csirkemell', 144, 200),
(3, 'Kolbász', 'Házi csemege kolbász', 392, 200),
(4, 'Szalámi', 'Szeletelt paprikás szalámi', 524, 200),
(5, 'Bacon', 'Elősütött, ropogós, kockázott prémium baconszalonna', 556, 200),
(6, 'Csirkemáj ragu', 'Házilag főzött csirkemájragu', 101, 200),
(7, 'Bolognai ragu', 'Házilag készített bolognai stílusú ragu', 223, 200),
(8, 'Pármai sonka', 'Lágy szeletekre dolgozott pármai sonka', 225, 200),
(9, 'Csípős kolbász', 'Házi csípős, paprikás kolbász', 215, 200),
(10, 'Tenger gyümölcse', 'Frissen feldolgozott tengeri herkentyűk', 58, 200),
(11, 'Tarja', 'Füstölt, főtt tarja', 153, 200),
(12, '[nincs megadva]', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kedvezmenyek`
--

CREATE TABLE `kedvezmenyek` (
  `ID` int(11) NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `szazalek` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `kedvezmenyek`
--

INSERT INTO `kedvezmenyek` (`ID`, `min`, `max`, `szazalek`) VALUES
(1, 100, 1000, 5);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kosar`
--

CREATE TABLE `kosar` (
  `ID` int(11) NOT NULL,
  `vasarloID` int(11) NOT NULL,
  `termekID` int(11) NOT NULL,
  `mennyiseg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `meretek`
--

CREATE TABLE `meretek` (
  `ID` int(11) NOT NULL,
  `meret` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `meretek`
--

INSERT INTO `meretek` (`ID`, `meret`, `ar`) VALUES
(1, '32cm', 1),
(2, '55cm', 3);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `pizzak`
--

CREATE TABLE `pizzak` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `meretID` int(11) NOT NULL,
  `tesztaID` int(11) NOT NULL,
  `szoszID` int(11) NOT NULL,
  `husID` int(11) DEFAULT NULL,
  `zoldsegID` int(11) DEFAULT NULL,
  `sajtID` int(11) DEFAULT NULL,
  `rendeltdb` int(11) NOT NULL,
  `img` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `pizzak`
--

INSERT INTO `pizzak` (`ID`, `megnevezes`, `meretID`, `tesztaID`, `szoszID`, `husID`, `zoldsegID`, `sajtID`, `rendeltdb`, `img`) VALUES
(16, 'Sonkás-Kukoricás', 1, 1, 1, 1, 3, 1, 1, 0),
(17, 'Magyaros', 1, 1, 3, 3, 1, 2, 1, 0),
(18, 'Tejfölös', 1, 1, 2, 5, 6, 6, 1, 0),
(19, 'Csirkemájas pizza', 1, 1, 1, 6, 1, 1, 1, 0),
(20, 'Vegetáriánus Light Pizza', 1, 1, 1, 12, 11, 1, 1, 0),
(21, 'Mérges pizza', 1, 2, 3, 9, 9, 2, 1, 0),
(22, 'Tenger gyümölcsei pizza', 1, 3, 1, 10, 7, 1, 1, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `rendelesek`
--

CREATE TABLE `rendelesek` (
  `ID` int(11) NOT NULL,
  `datum` datetime NOT NULL,
  `vasarloID` int(11) NOT NULL,
  `szakacsID` int(11) NOT NULL,
  `szallitoID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `osszpontszam` int(11) NOT NULL,
  `vegosszeg` int(11) NOT NULL,
  `statusz` int(11) NOT NULL,
  `fizmod` int(11) NOT NULL,
  `szallitasidij` int(11) NOT NULL,
  `kedvezmenyID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `rendelesek`
--

INSERT INTO `rendelesek` (`ID`, `datum`, `vasarloID`, `szakacsID`, `szallitoID`, `userID`, `osszpontszam`, `vegosszeg`, `statusz`, `fizmod`, `szallitasidij`, `kedvezmenyID`) VALUES
(5, '2020-03-18 14:09:00', 5, 4, 2, 2, 2, 2090, 1, 3, 1, 1),
(6, '2020-03-19 17:00:00', 5, 6, 3, 3, 10, 2210, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `rendelestetel`
--

CREATE TABLE `rendelestetel` (
  `ID` int(11) NOT NULL,
  `rendelesID` int(11) NOT NULL,
  `pizzaID` int(11) NOT NULL,
  `uditoID` int(11) NOT NULL,
  `mennyiseg` int(11) NOT NULL,
  `egysegar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sajtok`
--

CREATE TABLE `sajtok` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `kcal` int(11) DEFAULT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `sajtok`
--

INSERT INTO `sajtok` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Mozzarella sajt', 'Prémium minőségű mozzarella pizzafeltét', 300, 150),
(2, 'Trappista sajt', 'Prémium minőségű Trappista sajt', 381, 150),
(3, 'Pannónia sajt', 'Nagylyukú kiváló minőségű sajt', 394, 150),
(4, 'Márvány sajt', 'Intenzív, kéksajtnak is hívott különlegesség', 366, 150),
(5, 'Parmezán sajt', 'Finomra reszelt, inkább fűszernek használt sajtkülönlegesség', 385, 150),
(6, 'Camembert sajt', 'Tehéntejből készült Camembert sajt', 308, 150),
(7, 'Lapkasajt', 'Lapkasajt', 276, 150),
(8, 'Füstölt sajt', 'Füstölt sajt', 353, 150),
(9, '[nincs kiválasztva]', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `statuszok`
--

CREATE TABLE `statuszok` (
  `ID` int(11) NOT NULL,
  `statusz` varchar(20) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `statuszok`
--

INSERT INTO `statuszok` (`ID`, `statusz`) VALUES
(0, 'Inaktív'),
(1, 'Aktív'),
(3, 'Admin'),
(4, 'Moderátor');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `szoszok`
--

CREATE TABLE `szoszok` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `kcal` int(11) DEFAULT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `szoszok`
--

INSERT INTO `szoszok` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Paradicsomos szósz', 'Bazsalikommal fűszerezett paradicsomos pizza alap ', 60, 5),
(2, 'Tejfölös szósz', 'Borssal fűszerezett tejfölös pizza alap', 45, 5),
(3, 'Csípős szósz', 'Erős paprikakrémmel kevert paradicsomos pizza alap', 62, 5),
(4, 'Fokhagymás-Tejfölös szósz', 'Fokhagyma sűrítménnyel kevert tejfölös pizza alap', 51, 5),
(6, 'Barbecues alap', 'Házilag főzött barbecues alap', 133, 5),
(7, 'Mézes-chilis szósz', 'Mézes-chilis pizza alap', 119, 5),
(8, 'Csípős-tejfölös szósz', 'Erős paprikakrémmel kevert tejfölös pizza alap', 51, 5),
(9, '[nincs kiválasztva]', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tesztak`
--

CREATE TABLE `tesztak` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `kcal` int(11) NOT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `tesztak`
--

INSERT INTO `tesztak` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Normál tészta', 'Normál vastagságú peremes tészta (340-360g)', 340, 1090),
(2, 'Vastag tészta', 'Vastag, peremes tészta (470-490g)', 480, 1190),
(3, 'Vékony tészta', 'Vékony, perem nélküli tészta (180-200g)', 180, 1090),
(4, 'Teljes kiőrlésű tészta', 'Teljes kiőrlésű rozslisztből készített vékony tészta (180-200g)', 90, 1190);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `uditok`
--

CREATE TABLE `uditok` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `kcal` int(11) DEFAULT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `uditok`
--

INSERT INTO `uditok` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Coca Cola', 'Coca Cola 0,5l', 225, 400),
(2, 'Fanta Narancs', 'Fanta Narancs 0,5l', 140, 400),
(3, 'Sprite', 'Sprite 0,5l', 9, 400),
(4, 'Coca Cola', 'Coca Cola 1,25l', 563, 700),
(5, 'Fanta Narancs', 'Fanta Narancs 1,25l', 350, 800),
(6, 'Sprite', 'Sprite 1,25l', 113, 800),
(7, 'Nestea Barack', 'Barack ízesítésű jeges tea', 19, 800),
(8, 'Nestea Citrom', 'Citrom ízesítésű jeges tea', 19, 800);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `vasarlok`
--

CREATE TABLE `vasarlok` (
  `ID` int(11) NOT NULL,
  `nev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `tel` varchar(20) COLLATE utf8_hungarian_ci NOT NULL,
  `jelszo` varchar(32) COLLATE utf8_hungarian_ci NOT NULL,
  `regdatum` datetime NOT NULL,
  `last` datetime DEFAULT NULL,
  `vasalk` int(11) NOT NULL,
  `vasosszeg` int(11) NOT NULL,
  `penztarca` int(11) NOT NULL,
  `avatar` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `vasarlok`
--

INSERT INTO `vasarlok` (`ID`, `nev`, `email`, `tel`, `jelszo`, `regdatum`, `last`, `vasalk`, `vasosszeg`, `penztarca`, `avatar`, `status`) VALUES
(4, 'admin', 'admin', '36301234567', '21232f297a57a5a743894a0e4a801fc3', '2020-02-14 20:27:40', '2020-04-07 16:34:10', 0, 0, 0, '', 1),
(5, 'vasarlo', 'vasarlo', 'vasarlotel', 'ed650060631eae5ba2b3b1c72e5c67e2', '2020-02-27 00:00:00', '2020-02-23 14:06:02', 0, 0, 0, '', 1),
(8, 'vasarlo4', 'vasarlo4', 'vasarlo4', 'f591bf1b4decb98d39089f5d66261e07', '2020-04-05 12:57:16', '2020-04-05 12:57:23', 0, 0, 0, '1', 0),
(9, 'vasarlo12', 'vasarlo12', 'vasarlo12', '06bb21323d5ecc1ca334afbb597a236f', '2020-04-06 13:12:21', NULL, 0, 0, 0, '1', 0),
(10, 'vasarlo13', 'vasarlo13', 'vasarlo13', 'e61d89d45f0706fd8745d3e024ad9459', '2020-04-06 13:13:49', NULL, 0, 0, 0, '1', 0),
(11, 'vasarlo15', 'vasarlo15', 'vasarlo15', 'a9e62da78212745f646caeaf37f5dbe9', '2020-04-06 13:14:11', NULL, 0, 0, 0, '1', 0),
(12, 'vasarlo16', 'vasarlo16', 'vasarlo16', '0067c70761ddd7c87bae30ff0bc22ede', '2020-04-06 13:16:07', NULL, 0, 0, 0, '1', 0),
(13, 'vasarlo20', 'vasarlo20', 'vasarlo20', '9ab93060837a3f6bc0e642956eccff38', '2020-04-06 13:16:37', NULL, 0, 0, 0, '1', 0),
(14, 'vasarlo21', 'vasarlo21', 'vasarlo21', '66aa22194f778733d90c21a4765c329d', '2020-04-06 13:17:09', NULL, 0, 0, 0, '1', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `zoldsegek`
--

CREATE TABLE `zoldsegek` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `kcal` int(11) DEFAULT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `zoldsegek`
--

INSERT INTO `zoldsegek` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Hagyma', 'Vöröshagyma', 40, 100),
(3, 'Kukorica', 'Konzerv kukorica', 86, 100),
(4, 'Gomba', 'Csiperke gomba', 22, 100),
(5, 'Lilahagyma', 'Lilahagyma', 42, 100),
(6, 'Uborka', 'Csemegeuborka', 12, 100),
(7, 'Paradicsom', 'Szeletelt paradicsom', 18, 100),
(8, 'Brokkoli', 'Előpárolt brokkoli', 24, 100),
(9, 'Erős paprika', 'Szeletelt hegyes erős paprika', 5, 100),
(10, 'Kaliforniai paprika', 'Kockázott kaliforniai paprika', 26, 100),
(11, 'Ruccola', 'Ruccola', 25, 100),
(12, 'Pirított hagyma', 'Pirított hagyma', 332, 100),
(13, '[nincs kiválasztva]', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Nézet szerkezete `attekinto`
--
DROP TABLE IF EXISTS `attekinto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `attekinto`  AS  select `pizzak`.`ID` AS `ID`,`pizzak`.`megnevezes` AS `megnevezes`,`meretek`.`meret` AS `meret`,`tesztak`.`megnevezes` AS `teszta`,`szoszok`.`megnevezes` AS `szosz`,`szoszok`.`leiras` AS `szoszleiras`,`husok`.`megnevezes` AS `hus`,`husok`.`leiras` AS `husleiras`,`zoldsegek`.`megnevezes` AS `zoldseg`,`zoldsegek`.`leiras` AS `zoldsegleiras`,`sajtok`.`megnevezes` AS `sajt`,`sajtok`.`leiras` AS `sajtleiras`,`meretek`.`ar` * (`tesztak`.`ar` + `szoszok`.`ar` + `husok`.`ar` + `sajtok`.`ar` + `zoldsegek`.`ar`) AS `ar`,`tesztak`.`kcal` + `szoszok`.`kcal` + `husok`.`kcal` + `sajtok`.`kcal` + `zoldsegek`.`kcal` AS `kcal`,`pizzak`.`img` AS `kep` from ((((((`pizzak` join `meretek` on(`pizzak`.`meretID` = `meretek`.`ID`)) join `tesztak` on(`pizzak`.`tesztaID` = `tesztak`.`ID`)) join `szoszok` on(`pizzak`.`szoszID` = `szoszok`.`ID`)) join `husok` on(`pizzak`.`husID` = `husok`.`ID`)) join `sajtok` on(`pizzak`.`sajtID` = `sajtok`.`ID`)) join `zoldsegek` on(`pizzak`.`zoldsegID` = `zoldsegek`.`ID`)) WITH CASCADED CHECK OPTION ;

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `cimek`
--
ALTER TABLE `cimek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `vasarloID` (`vasarloID`);

--
-- A tábla indexei `desszertek`
--
ALTER TABLE `desszertek`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `felhasznalok`
--
ALTER TABLE `felhasznalok`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `statusz` (`statusz`);

--
-- A tábla indexei `fizmodok`
--
ALTER TABLE `fizmodok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `hirek`
--
ALTER TABLE `hirek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `felhasznaloID` (`felhasznaloID`);

--
-- A tábla indexei `husok`
--
ALTER TABLE `husok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `kedvezmenyek`
--
ALTER TABLE `kedvezmenyek`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `kosar`
--
ALTER TABLE `kosar`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `vasarloID` (`vasarloID`),
  ADD KEY `termekID` (`termekID`);

--
-- A tábla indexei `meretek`
--
ALTER TABLE `meretek`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `pizzak`
--
ALTER TABLE `pizzak`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `tesztaID` (`tesztaID`),
  ADD KEY `szoszID` (`szoszID`),
  ADD KEY `husID` (`husID`),
  ADD KEY `zoldsegID` (`zoldsegID`),
  ADD KEY `sajtID` (`sajtID`),
  ADD KEY `meret` (`meretID`);

--
-- A tábla indexei `rendelesek`
--
ALTER TABLE `rendelesek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `vasarloID` (`vasarloID`),
  ADD KEY `szakacsID` (`szakacsID`),
  ADD KEY `szallitoID` (`szallitoID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `fizmod` (`fizmod`),
  ADD KEY `szallitasidij` (`szallitasidij`),
  ADD KEY `kedvezmenyID` (`kedvezmenyID`);

--
-- A tábla indexei `rendelestetel`
--
ALTER TABLE `rendelestetel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `rendelesID` (`rendelesID`),
  ADD KEY `pizzaID` (`pizzaID`),
  ADD KEY `uditoID` (`uditoID`);

--
-- A tábla indexei `sajtok`
--
ALTER TABLE `sajtok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `statuszok`
--
ALTER TABLE `statuszok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `szoszok`
--
ALTER TABLE `szoszok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `tesztak`
--
ALTER TABLE `tesztak`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `uditok`
--
ALTER TABLE `uditok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `vasarlok`
--
ALTER TABLE `vasarlok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `zoldsegek`
--
ALTER TABLE `zoldsegek`
  ADD PRIMARY KEY (`ID`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `cimek`
--
ALTER TABLE `cimek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT a táblához `desszertek`
--
ALTER TABLE `desszertek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT a táblához `felhasznalok`
--
ALTER TABLE `felhasznalok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT a táblához `fizmodok`
--
ALTER TABLE `fizmodok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `hirek`
--
ALTER TABLE `hirek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `husok`
--
ALTER TABLE `husok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT a táblához `kedvezmenyek`
--
ALTER TABLE `kedvezmenyek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `kosar`
--
ALTER TABLE `kosar`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT a táblához `meretek`
--
ALTER TABLE `meretek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `pizzak`
--
ALTER TABLE `pizzak`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT a táblához `rendelesek`
--
ALTER TABLE `rendelesek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT a táblához `rendelestetel`
--
ALTER TABLE `rendelestetel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `sajtok`
--
ALTER TABLE `sajtok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT a táblához `statuszok`
--
ALTER TABLE `statuszok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT a táblához `szoszok`
--
ALTER TABLE `szoszok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT a táblához `tesztak`
--
ALTER TABLE `tesztak`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT a táblához `uditok`
--
ALTER TABLE `uditok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT a táblához `vasarlok`
--
ALTER TABLE `vasarlok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT a táblához `zoldsegek`
--
ALTER TABLE `zoldsegek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `cimek`
--
ALTER TABLE `cimek`
  ADD CONSTRAINT `cimek_ibfk_1` FOREIGN KEY (`vasarloID`) REFERENCES `vasarlok` (`ID`);

--
-- Megkötések a táblához `felhasznalok`
--
ALTER TABLE `felhasznalok`
  ADD CONSTRAINT `felhasznalok_ibfk_1` FOREIGN KEY (`statusz`) REFERENCES `statuszok` (`ID`);

--
-- Megkötések a táblához `hirek`
--
ALTER TABLE `hirek`
  ADD CONSTRAINT `hirek_ibfk_1` FOREIGN KEY (`felhasznaloID`) REFERENCES `felhasznalok` (`ID`);

--
-- Megkötések a táblához `kosar`
--
ALTER TABLE `kosar`
  ADD CONSTRAINT `kosar_ibfk_1` FOREIGN KEY (`vasarloID`) REFERENCES `vasarlok` (`ID`);

--
-- Megkötések a táblához `pizzak`
--
ALTER TABLE `pizzak`
  ADD CONSTRAINT `pizzak_ibfk_1` FOREIGN KEY (`tesztaID`) REFERENCES `tesztak` (`ID`),
  ADD CONSTRAINT `pizzak_ibfk_2` FOREIGN KEY (`szoszID`) REFERENCES `szoszok` (`ID`),
  ADD CONSTRAINT `pizzak_ibfk_3` FOREIGN KEY (`husID`) REFERENCES `husok` (`ID`),
  ADD CONSTRAINT `pizzak_ibfk_4` FOREIGN KEY (`zoldsegID`) REFERENCES `zoldsegek` (`ID`),
  ADD CONSTRAINT `pizzak_ibfk_5` FOREIGN KEY (`sajtID`) REFERENCES `sajtok` (`ID`),
  ADD CONSTRAINT `pizzak_ibfk_6` FOREIGN KEY (`meretID`) REFERENCES `meretek` (`ID`);

--
-- Megkötések a táblához `rendelesek`
--
ALTER TABLE `rendelesek`
  ADD CONSTRAINT `rendelesek_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `felhasznalok` (`ID`),
  ADD CONSTRAINT `rendelesek_ibfk_2` FOREIGN KEY (`vasarloID`) REFERENCES `vasarlok` (`ID`),
  ADD CONSTRAINT `rendelesek_ibfk_3` FOREIGN KEY (`fizmod`) REFERENCES `fizmodok` (`ID`),
  ADD CONSTRAINT `rendelesek_ibfk_5` FOREIGN KEY (`szakacsID`) REFERENCES `felhasznalok` (`ID`),
  ADD CONSTRAINT `rendelesek_ibfk_6` FOREIGN KEY (`szallitoID`) REFERENCES `felhasznalok` (`ID`),
  ADD CONSTRAINT `rendelesek_ibfk_7` FOREIGN KEY (`kedvezmenyID`) REFERENCES `kedvezmenyek` (`ID`);

--
-- Megkötések a táblához `rendelestetel`
--
ALTER TABLE `rendelestetel`
  ADD CONSTRAINT `rendelestetel_ibfk_1` FOREIGN KEY (`rendelesID`) REFERENCES `rendelesek` (`ID`),
  ADD CONSTRAINT `rendelestetel_ibfk_2` FOREIGN KEY (`pizzaID`) REFERENCES `pizzak` (`ID`),
  ADD CONSTRAINT `rendelestetel_ibfk_3` FOREIGN KEY (`uditoID`) REFERENCES `uditok` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
