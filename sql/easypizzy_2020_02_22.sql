-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2020. Feb 22. 19:02
-- Kiszolgáló verziója: 10.4.11-MariaDB
-- PHP verzió: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `zd_dajkan`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `cimek`
--

CREATE TABLE `cimek` (
  `ID` int(11) NOT NULL,
  `vasarloID` int(11) NOT NULL,
  `irsz` int(11) NOT NULL,
  `lakcim` varchar(100) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `felhasznalok`
--

CREATE TABLE `felhasznalok` (
  `ID` int(11) NOT NULL,
  `nev` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `jelszo` varchar(32) COLLATE utf8_hungarian_ci NOT NULL,
  `tel` varchar(20) COLLATE utf8_hungarian_ci NOT NULL,
  `beosztas` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(11) NOT NULL,
  `pontszam` int(11) NOT NULL,
  `profil` int(11) NOT NULL,
  `avatar` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `regdatum` datetime NOT NULL,
  `utbelepdatum` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `felhasznalok`
--

INSERT INTO `felhasznalok` (`ID`, `nev`, `email`, `jelszo`, `tel`, `beosztas`, `statusz`, `pontszam`, `profil`, `avatar`, `regdatum`, `utbelepdatum`) VALUES
(2, 'Futár', 'futar@easypizzy.hu', '21232f297a57a5a743894a0e4a801fc3', '+3612345678', 'Futár', 1, 0, 0, '<img src=\"images/avatar/nopic.jpg\">', '2020-02-14 00:00:00', '2020-02-14 21:05:00'),
(3, 'Futár2', 'futar2@easypizzy.hu', '21232f297a57a5a743894a0e4a801fc3', '+3612345678', 'Futár', 1, 0, 0, '', '2020-02-14 00:00:00', '2020-02-14 21:04:00'),
(4, 'Szakács', 'szakacs@easypizzy.hu', '21232f297a57a5a743894a0e4a801fc3', '+3612345678', 'Szakács', 1, 0, 0, '', '2020-02-14 00:00:00', '2020-02-14 00:00:00'),
(5, 'Szakács2', 'szakacs2@easypizzy.hu', '21232f297a57a5a743894a0e4a801fc3', '+3612345678', 'Szakács', 1, 0, 0, '', '2020-02-14 00:00:00', '2020-02-14 00:00:00'),
(6, 'Üzletvezető', 'uzletvezeto@easypizzy.hu', '21232f297a57a5a743894a0e4a801fc3', '+3654456585', 'Üzletvezető', 1, 0, 0, '', '2020-02-14 00:00:00', '2020-02-14 00:00:00'),
(7, 'ADMIN', 'admin', '21232f297a57a5a743894a0e4a801fc3', '+36', 'ADMIN', 1, 0, 0, '0', '2020-02-22 00:00:00', '2020-02-22 00:00:00');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `fizmodok`
--

CREATE TABLE `fizmodok` (
  `ID` int(11) NOT NULL,
  `fizmod` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `husok`
--

CREATE TABLE `husok` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `kcal` int(11) NOT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `husok`
--

INSERT INTO `husok` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Sonka', 'Pizzasonka lágy szeletekre feldolgozva', 155, 200),
(2, 'Csirkemell', 'Elősütött, fűszerezett csirkemell', 144, 200),
(3, 'Kolbász', 'Házi csemege kolbász', 392, 200),
(4, 'Szalámi', 'Szeletelt paprikás szalámi', 524, 200),
(5, 'Bacon', 'Elősütött, ropogós, kockázott prémium baconszalonna', 556, 200),
(6, 'Csirkemáj ragu', 'Házilag főzött csirkemájragu', 101, 200);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kedvezmenyek`
--

CREATE TABLE `kedvezmenyek` (
  `ID` int(11) NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `szazalek` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `meretek`
--

CREATE TABLE `meretek` (
  `ID` int(11) NOT NULL,
  `meret` int(11) NOT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `meretek`
--

INSERT INTO `meretek` (`ID`, `meret`, `ar`) VALUES
(1, 32, 1090),
(2, 55, 1090);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `pizzak`
--

CREATE TABLE `pizzak` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `meret` int(11) NOT NULL,
  `tesztaID` int(11) NOT NULL,
  `szoszID` int(11) NOT NULL,
  `husID` int(11) NOT NULL,
  `zoldsegID` int(11) NOT NULL,
  `sajtID` int(11) NOT NULL,
  `rendeltdb` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `rendelesek`
--

CREATE TABLE `rendelesek` (
  `ID` int(11) NOT NULL,
  `datum` datetime NOT NULL,
  `vasarloID` int(11) NOT NULL,
  `szakacsID` int(11) NOT NULL,
  `szallitoID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `osszpontszam` int(11) NOT NULL,
  `vegosszeg` int(11) NOT NULL,
  `statusz` int(11) NOT NULL,
  `fizmod` int(11) NOT NULL,
  `szallitasidij` int(11) NOT NULL,
  `kedvezmenyID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `rendelestetel`
--

CREATE TABLE `rendelestetel` (
  `ID` int(11) NOT NULL,
  `rendelesID` int(11) NOT NULL,
  `pizzaID` int(11) NOT NULL,
  `uditoID` int(11) NOT NULL,
  `mennyiseg` int(11) NOT NULL,
  `egysegar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sajtok`
--

CREATE TABLE `sajtok` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `kcal` int(11) NOT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `sajtok`
--

INSERT INTO `sajtok` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Mozzarella sajt', 'Prémium minőségű mozzarella pizzafeltét', 300, 150),
(2, 'Trappista sajt', 'Prémium minőségű Trappista sajt', 381, 150),
(3, 'Pannónia sajt', 'Nagylyukú kiváló minőségű sajt', 394, 150),
(4, 'Márvány sajt', 'Intenzív, kéksajtnak is hívott különlegesség', 366, 150),
(5, 'Parmezán sajt', 'Finomra reszelt, inkább fűszernek használt sajtkülönlegesség', 385, 150),
(6, 'Camembert sajt', 'Tehéntejből készült Camembert sajt', 308, 150);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `statuszok`
--

CREATE TABLE `statuszok` (
  `ID` int(11) NOT NULL,
  `statusz` varchar(20) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `statuszok`
--

INSERT INTO `statuszok` (`ID`, `statusz`) VALUES
(0, 'Inaktív'),
(1, 'Aktív'),
(3, 'Admin');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `szallitasidijak`
--

CREATE TABLE `szallitasidijak` (
  `ID` int(11) NOT NULL,
  `megnev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `osszeg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `szoszok`
--

CREATE TABLE `szoszok` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `kcal` int(11) NOT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `szoszok`
--

INSERT INTO `szoszok` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Paradicsomos szósz', 'Bazsalikommal fűszerezett paradicsomos pizza alap ', 60, 5),
(2, 'Tejfölös szósz', 'Borssal fűszerezett tejfölös pizza alap', 45, 5),
(3, 'Csípős szósz', 'Erős paprikakrémmel kevert paradicsomos pizza alap', 62, 5),
(4, 'Fokhagymás-Tejfölös szósz', 'Fokhagyma sűrítménnyel kevert tejfölös pizza alap', 51, 5);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `telepulesek`
--

CREATE TABLE `telepulesek` (
  `irsz` int(11) NOT NULL,
  `telepulesnev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `telepulesek`
--

INSERT INTO `telepulesek` (`irsz`, `telepulesnev`) VALUES
(7100, 'Szekszárd'),
(7121, 'Szálka'),
(7122, 'Kakasd'),
(7130, 'Tolna'),
(7136, 'Fácánkert'),
(7139, 'Fadd'),
(7140, 'Bátaszék'),
(7143, 'Őcsény'),
(7144, 'Decs'),
(7146, 'Várdomb');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tesztak`
--

CREATE TABLE `tesztak` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `kcal` int(11) NOT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `tesztak`
--

INSERT INTO `tesztak` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Normál tészta', 'Normál vastagságú peremes tészta (340-360g)', 340, 1090),
(2, 'Vastag tészta', 'Vastag, peremes tészta (480-490g)', 480, 1190),
(3, 'Vékony tészta', 'Vékony, perem nélküli tészta (180-200g)', 180, 1090),
(4, 'Teljes kiőrlésű tészta', 'Teljes kiőrlésű rozslisztből készített vékony tészta (180-200g)', 90, 1190);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `uditok`
--

CREATE TABLE `uditok` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `kcal` int(11) NOT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `uditok`
--

INSERT INTO `uditok` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Coca Cola', 'Coca Cola 0,5l', 225, 400),
(2, 'Fanta Narancs', 'Fanta Narancs 0,5l', 140, 400),
(3, 'Sprite', 'Sprite 0,5l', 400, 9),
(4, 'Coca Cola', 'Coca Cola 1,25l', 563, 700),
(5, 'Fanta Narancs', 'Fanta Narancs 1,25l', 350, 800),
(6, 'Sprite', 'Sprite 1,25l', 113, 800);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `vasarlok`
--

CREATE TABLE `vasarlok` (
  `ID` int(11) NOT NULL,
  `nev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `tel` varchar(20) COLLATE utf8_hungarian_ci NOT NULL,
  `jelszo` varchar(32) COLLATE utf8_hungarian_ci NOT NULL,
  `regdatum` datetime NOT NULL,
  `last` datetime DEFAULT NULL,
  `vasalk` int(11) NOT NULL,
  `vasosszeg` int(11) NOT NULL,
  `penztarca` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `vasarlok`
--

INSERT INTO `vasarlok` (`ID`, `nev`, `email`, `tel`, `jelszo`, `regdatum`, `last`, `vasalk`, `vasosszeg`, `penztarca`, `status`) VALUES
(4, 'admin', 'admin@admin.hu', '+36301234567', '21232f297a57a5a743894a0e4a801fc3', '2020-02-14 20:27:40', '2020-02-22 16:40:43', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `zoldsegek`
--

CREATE TABLE `zoldsegek` (
  `ID` int(11) NOT NULL,
  `megnevezes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `kcal` int(11) NOT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `zoldsegek`
--

INSERT INTO `zoldsegek` (`ID`, `megnevezes`, `leiras`, `kcal`, `ar`) VALUES
(1, 'Hagyma', 'Vöröshagyma', 40, 100),
(3, 'Kukorica', 'Konzerv kukorica', 86, 100),
(4, 'Gomba', 'Csiperke gomba', 22, 100),
(5, 'Lilahagyma', 'Lilahagyma', 42, 100),
(6, 'Uborka', 'Csemegeuborka', 12, 100);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `cimek`
--
ALTER TABLE `cimek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `vasarloID` (`vasarloID`),
  ADD KEY `irsz` (`irsz`);

--
-- A tábla indexei `felhasznalok`
--
ALTER TABLE `felhasznalok`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `statusz` (`statusz`);

--
-- A tábla indexei `fizmodok`
--
ALTER TABLE `fizmodok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `husok`
--
ALTER TABLE `husok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `kedvezmenyek`
--
ALTER TABLE `kedvezmenyek`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `meretek`
--
ALTER TABLE `meretek`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `pizzak`
--
ALTER TABLE `pizzak`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `tesztaID` (`tesztaID`),
  ADD KEY `szoszID` (`szoszID`),
  ADD KEY `husID` (`husID`),
  ADD KEY `zoldsegID` (`zoldsegID`),
  ADD KEY `sajtID` (`sajtID`),
  ADD KEY `meret` (`meret`);

--
-- A tábla indexei `rendelesek`
--
ALTER TABLE `rendelesek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `vasarloID` (`vasarloID`),
  ADD KEY `szakacsID` (`szakacsID`),
  ADD KEY `szallitoID` (`szallitoID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `fizmod` (`fizmod`),
  ADD KEY `szallitasidij` (`szallitasidij`),
  ADD KEY `kedvezmenyID` (`kedvezmenyID`);

--
-- A tábla indexei `rendelestetel`
--
ALTER TABLE `rendelestetel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `rendelesID` (`rendelesID`),
  ADD KEY `pizzaID` (`pizzaID`),
  ADD KEY `uditoID` (`uditoID`);

--
-- A tábla indexei `sajtok`
--
ALTER TABLE `sajtok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `statuszok`
--
ALTER TABLE `statuszok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `szallitasidijak`
--
ALTER TABLE `szallitasidijak`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `szoszok`
--
ALTER TABLE `szoszok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `telepulesek`
--
ALTER TABLE `telepulesek`
  ADD PRIMARY KEY (`irsz`);

--
-- A tábla indexei `tesztak`
--
ALTER TABLE `tesztak`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `uditok`
--
ALTER TABLE `uditok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `vasarlok`
--
ALTER TABLE `vasarlok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `zoldsegek`
--
ALTER TABLE `zoldsegek`
  ADD PRIMARY KEY (`ID`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `cimek`
--
ALTER TABLE `cimek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `felhasznalok`
--
ALTER TABLE `felhasznalok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT a táblához `fizmodok`
--
ALTER TABLE `fizmodok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `husok`
--
ALTER TABLE `husok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT a táblához `kedvezmenyek`
--
ALTER TABLE `kedvezmenyek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `meretek`
--
ALTER TABLE `meretek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `pizzak`
--
ALTER TABLE `pizzak`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `rendelesek`
--
ALTER TABLE `rendelesek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `rendelestetel`
--
ALTER TABLE `rendelestetel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `sajtok`
--
ALTER TABLE `sajtok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT a táblához `statuszok`
--
ALTER TABLE `statuszok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `szallitasidijak`
--
ALTER TABLE `szallitasidijak`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `szoszok`
--
ALTER TABLE `szoszok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT a táblához `tesztak`
--
ALTER TABLE `tesztak`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT a táblához `uditok`
--
ALTER TABLE `uditok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT a táblához `vasarlok`
--
ALTER TABLE `vasarlok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT a táblához `zoldsegek`
--
ALTER TABLE `zoldsegek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `cimek`
--
ALTER TABLE `cimek`
  ADD CONSTRAINT `cimek_ibfk_1` FOREIGN KEY (`vasarloID`) REFERENCES `vasarlok` (`ID`),
  ADD CONSTRAINT `cimek_ibfk_2` FOREIGN KEY (`irsz`) REFERENCES `telepulesek` (`irsz`);

--
-- Megkötések a táblához `felhasznalok`
--
ALTER TABLE `felhasznalok`
  ADD CONSTRAINT `felhasznalok_ibfk_1` FOREIGN KEY (`statusz`) REFERENCES `statuszok` (`ID`);

--
-- Megkötések a táblához `pizzak`
--
ALTER TABLE `pizzak`
  ADD CONSTRAINT `pizzak_ibfk_1` FOREIGN KEY (`tesztaID`) REFERENCES `tesztak` (`ID`),
  ADD CONSTRAINT `pizzak_ibfk_2` FOREIGN KEY (`szoszID`) REFERENCES `szoszok` (`ID`),
  ADD CONSTRAINT `pizzak_ibfk_3` FOREIGN KEY (`husID`) REFERENCES `husok` (`ID`),
  ADD CONSTRAINT `pizzak_ibfk_4` FOREIGN KEY (`zoldsegID`) REFERENCES `zoldsegek` (`ID`),
  ADD CONSTRAINT `pizzak_ibfk_5` FOREIGN KEY (`sajtID`) REFERENCES `sajtok` (`ID`),
  ADD CONSTRAINT `pizzak_ibfk_6` FOREIGN KEY (`meret`) REFERENCES `meretek` (`ID`);

--
-- Megkötések a táblához `rendelesek`
--
ALTER TABLE `rendelesek`
  ADD CONSTRAINT `rendelesek_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `felhasznalok` (`ID`),
  ADD CONSTRAINT `rendelesek_ibfk_2` FOREIGN KEY (`vasarloID`) REFERENCES `vasarlok` (`ID`),
  ADD CONSTRAINT `rendelesek_ibfk_3` FOREIGN KEY (`fizmod`) REFERENCES `fizmodok` (`ID`),
  ADD CONSTRAINT `rendelesek_ibfk_4` FOREIGN KEY (`szallitasidij`) REFERENCES `szallitasidijak` (`ID`),
  ADD CONSTRAINT `rendelesek_ibfk_5` FOREIGN KEY (`szakacsID`) REFERENCES `felhasznalok` (`ID`),
  ADD CONSTRAINT `rendelesek_ibfk_6` FOREIGN KEY (`szallitoID`) REFERENCES `felhasznalok` (`ID`),
  ADD CONSTRAINT `rendelesek_ibfk_7` FOREIGN KEY (`kedvezmenyID`) REFERENCES `kedvezmenyek` (`ID`);

--
-- Megkötések a táblához `rendelestetel`
--
ALTER TABLE `rendelestetel`
  ADD CONSTRAINT `rendelestetel_ibfk_1` FOREIGN KEY (`rendelesID`) REFERENCES `rendelesek` (`ID`),
  ADD CONSTRAINT `rendelestetel_ibfk_2` FOREIGN KEY (`pizzaID`) REFERENCES `pizzak` (`ID`),
  ADD CONSTRAINT `rendelestetel_ibfk_3` FOREIGN KEY (`uditoID`) REFERENCES `uditok` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
