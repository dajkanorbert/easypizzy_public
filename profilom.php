<h3>Profilom</h3>
<hr>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fas fa-trophy"></i></div>
		<div class="boxcontent">
			<h3>EasyPizzy krediteim</h3>
			<p>Kövesd összegyűjtött krediteid</p>
			<a href="index.php?pg=szemelyes/kreditek">INFÓ</a>
		</div>
	</div>
</div>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fas fa-coins"></i></div>
		<div class="boxcontent">
			<h3>ÉRTÉKELÉSEK</h3>
			<p>Rendelés utáni kérdőívek</p>
			<a href="index.php?pg=szemelyes/ertekeles">ÉRTÉKELEK</a>
		</div>
	</div>
</div>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fas fa-hotel"></i></div>
		<div class="boxcontent">
			<h3>CÍMEIM</h3>
			<p>Szállítási címeid nyilvántartása</p>
			<a href="index.php?pg=szemelyes/cimek">MEGNÉZEM</a>
		</div>
	</div>
</div>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fas fa-user-cog"></i></div>
		<div class="boxcontent">
			<h3>ADATAIM</h3>
			<p>Módosíthatod személyes adataid</p>
			<a href="index.php?pg=szemelyes/adataim">MÓDOSÍTOM</a>
		</div>
	</div>
</div>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fas fa-key"></i></div>
		<div class="boxcontent">
			<h3>JELSZÓKEZELÉS</h3>
			<p>Itt módosíthatod jelszavad</p>
			<a href="index.php?pg=szemelyes/jelszomod">MÓDOSÍTOM</a>
		</div>
	</div>
</div>
