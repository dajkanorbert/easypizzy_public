<h3>Desszertek kezelése</h3>
<hr>

<!-- Új desszert hozzáadása gomb, átírányít felvétel oldalra -->
<a href="?pg=desszertek_felv" class="btn btn-primary">Új desszert hozzáadása</a>
<?php

	// kilistázza az összes desszertet táblázatos formában
	$db->query("SELECT ID AS 'Azonosító' , megnevezes AS 'Megnevezés' , leiras AS 'Leírás' , kcal AS 'Kcal' , ar AS 'Ár' , img AS 'Kép' FROM desszertek WHERE ID<>27");
	$db->convertTable('i|u|d');
?>

<a href="?pg=menu/torzsadatok_kezelese" class="btn btn-primary">Vissza a törzsadatok kezeléséhez</a>