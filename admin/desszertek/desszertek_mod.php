<h3>Desszert módosítása</h3>
<hr>
<?php
	//a módosítandó termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintottunk már a módosít gombra akkor innen fut a program
	if (isset($_POST['modosit']))
	{
		//változóba mentjük a beírt adatokat és az escapeshellcmd-vel levédjük sql injection támadás ellen
		$megnevezes = escapeshellcmd($_POST['megnevezes']);
		$leiras = escapeshellcmd($_POST['leiras']);
		$kaloria = escapeshellcmd($_POST['kaloria']);
		$ar = escapeshellcmd($_POST['ar']);

		$avatar=$_FILES['fileToUpload']['name'];

		//ha üres valamelyik kötelezően kitöltendő mezőnk akkor írjon ki hibaszöveget
		if (empty($megnevezes) || empty($ar))
		{
			showError('Nem adtál meg minden adatot!');
		}
		else
		{
			//lekérdezzük megnevezés alapján és, ha 0-nál több találatot ad vissza akkor a termék már létezik az adatbázisban
			$db->query("SELECT ID FROM desszertek WHERE megnevezes='$megnevezes' AND ID<>$id");
			if ($db->numRows() != 0)
			{
				showError('Van már ilyen nevű desszert az adatbázisban!');
			}
			else
			{
				//ha minden adat rendben van akkor módosítjuk a terméket és visszaírányítjuk a desszertekhez
				$db->query("UPDATE desszertek SET megnevezes='$megnevezes', leiras='$leiras', kcal=$kaloria, ar=$ar WHERE ID=$id");

				// Képfeltöltés
			    if(!empty($avatar))
			    {
			      uploadFile($_FILES['fileToUpload'],'target:../termekkepek/avatar|maxsize:2|allow:jpg,png,bmp,jpeg|filename:'.$avatar);
			      $db->query("UPDATE desszertek SET img='$avatar' WHERE ID=$id");
			    }


				header("location: index.php?pg=desszertek/desszertek");
			}
		}
	}

	//lekérdezzük a módosítandó termék jelenlegi paramétereit és feltöltjük vele az űrlapot
	$db->query("SELECT * FROM desszertek WHERE ID=$id");
	$desszertek = $db->fetchAll();

	echo '
	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
	<form method="POST" action="index.php?pg=desszertek_mod&id='.$id.'" enctype="multipart/form-data">
		<div class="form-group">
			<label>Megnevezés: *</label>
			<input type="text" name="megnevezes" class="form-control" value="'.$desszertek[0]['megnevezes'].'">
		</div>
			<div class="form-group">
			<label>Leírás:</label>
			<textarea name="leiras" class="form-control">'.$desszertek[0]['leiras'].'</textarea>
		</div>
		<div class="form-group">
			<label>Kalória:</label>
			<input type="number" name="kaloria" class="form-control" value="'.$desszertek[0]['kcal'].'">
		</div>
		<div class="form-group">
			<label>Ár: *</label>
			<input type="number" name="ar" class="form-control" value="'.$desszertek[0]['ar'].'">
		</div>

		<div class="form-group">
			<label>Termékkép</label><br>';

		if (empty($desszertek[0]['img']))
		{
		  echo '<input type="file" name="fileToUpload" id="fileToUpload"><br />';
		}
		else
		{
		  print ($desszertek[0]['img']);

		  echo'
		  <a href="index.php?pg=desszertek/avatardel&id='.$id.'" class="btn btn-danger">Profilkép törlése</a><br><br> ';
		}

		echo '
		<div class="form-group">
			<input type="submit" name="modosit" value="Módosít" class="btn btn-primary">
			<a href="?pg=desszertek" class="btn btn-primary">Vissza a desszertek listájához</a>
			<br><br>
			A *-al jelölt adatok megadása kötelező!
		</div>
	</form>
	</div>
	<div class="col-xs-12 col-sm-3"></div>
	';
?>