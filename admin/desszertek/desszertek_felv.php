<h3>Új desszert hozzáadása</h3>
<hr>
<?php
	//ellenőrzi, hogy rákattintottunk-e a felvesz gombra
	if (isset($_POST['felvesz']))
	{
		//változóba mentjük a beírt adatokat és az escapeshellcmd-vel levédjük sql injection támadás ellen
		$megnevezes = escapeshellcmd($_POST['megnevezes']);
		$leiras = escapeshellcmd($_POST['leiras']);
		$kaloria = escapeshellcmd($_POST['kaloria']);
		$ar = escapeshellcmd($_POST['ar']);

		$avatar=$_FILES['fileToUpload']['name'];

		//ha üres valamelyik kötelezően kitöltendő mezőnk akkor írjon ki hibaszöveget
		if (empty($megnevezes) || empty($ar))
		{
			showError('Nem adtál meg minden adatot!');
		}
		else
		{
			//lekérdezzük megnevezés alapján és, ha 0-nál több találatot ad vissza akkor a termék már létezik az adatbázisban
			$db->query("SELECT ID FROM desszertek WHERE megnevezes='$megnevezes'");
			if ($db->numRows() != 0)
			{
				showError('Van már ilyen nevű desszert az adatbázisban!');
			}
			else
			{
				//ha minden adat rendben van akkor beszúrjuk a táblába a terméket és visszaírányítjuk a desszertekhez
				$db->query("INSERT INTO desszertek VALUES(null, '$megnevezes', '$leiras', $kaloria, $ar,'',0)");

				// Képfeltöltés
			    if(!empty($avatar))
			    {
			      uploadFile($_FILES['fileToUpload'],'target:../termekkepek/avatar|maxsize:2|allow:jpg,png,bmp,jpeg|filename:'.$avatar);
			      $id = $db->LastID();
			      $db->query("UPDATE desszertek SET img=
			      	'<a href=\"termekkepek/avatar/".$avatar."\" data-lightbox=\"X\"><img src=\"termekkepek/avatar/".$avatar."\" class=\"img-thumbnail img-rounded imghover img-responsive c img avatar\"></a>' WHERE ID=$id");

			    }

				header("location: index.php?pg=desszertek");
			}
		}

	}


	//<!-- Űrlap új adat felvételéhez -->

echo '
<div class="col-xs-12 col-sm-3"></div>
<div class="col-xs-12 col-sm-6">
<form method="POST" action="?pg=desszertek_felv" enctype="multipart/form-data">
	<div class="form-group">
		<label>Megnevezés: *</label>
		<input type="text" name="megnevezes" class="form-control">
	</div>
		<div class="form-group">
		<label>Leírás:</label>
		<textarea name="leiras" class="form-control"></textarea>
	</div>
	<div class="form-group">
		<label>Kalória:</label>
		<input type="number" name="kaloria" class="form-control">
	</div>
	<div class="form-group">
		<label>Ár: *</label>
		<input type="number" name="ar" class="form-control">
	</div>
	<div class="form-group">
			<label>Termékkép</label><br>';

		if (empty($avatar))
		{
		  echo '<input type="file" name="fileToUpload" id="fileToUpload"><br />';
		}
		else
		{
		  echo'<img src="termekkepek/avatar/'.$avatar.'" class="img img-thumbnail"><br>
		  <a href="index.php?pg=desszertek/avatardel" class="btn btn-danger">Termékkép törlése</a><br><br> ';
		}

		echo '
		</div>
	<div class="form-group">
		<input type="submit" name="felvesz" value="Felvesz" class="btn btn-primary">
		<a href="?pg=desszertek" class="btn btn-primary">Vissza a desszertek listájához</a>
		<br><br>
		A *-al jelölt adatok megadása kötelező!
	</div>
</form>
</div>
<div class="col-xs-12 col-sm-3"></div>
';