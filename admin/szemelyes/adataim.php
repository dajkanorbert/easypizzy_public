<h3>Adataim módosítása</h3>
<hr>
<?php
	$id = $_SESSION['fid'];

	if (isset($_POST['modosit']))
	{
		$nev = escapeshellcmd($_POST['nev']);
		$email = escapeshellcmd($_POST['email']);
		$tel = escapeshellcmd($_POST['tel']);
		$beosztas = escapeshellcmd($_POST['beosztas']);
		$status = escapeshellcmd($_POST['status']);

		$avatar=$_FILES['fileToUpload']['name'];

		if (empty($nev) || empty($email) || empty($tel))
		{
			showError('Nem adtál meg minden adatot!');
		}
		else
		{
			$db->query("SELECT email FROM felhasznalok WHERE email='$email' AND ID<>$id");
			if ($db->numRows() != 0)
			{
				showError('Ez az e-mail cím már foglalt!');
			}
			else
			{
				$db->query("UPDATE felhasznalok SET nev='$nev', email='$email', tel='$tel' , beosztas='$beosztas' , status='$status'  WHERE ID=$id");

			    // Képfeltöltés
			    if(!empty($avatar))
			    {
			      uploadFile($_FILES['fileToUpload'],'target:images/avatar|maxsize:2|allow:jpg,png,bmp,jpeg|filename:'.$avatar);
			      $db->query("UPDATE felhasznalok SET avatar='$avatar' WHERE ID=$id");
			      $_SESSION['fpic'] = $avatar;
			    }
				header("location:index.php?pg=szemelyes");		
			}

		}
	}
	
	$db->query("SELECT * FROM felhasznalok WHERE ID=$id");
	$felhasznalok = $db->fetchAll();

	if ($felhasznalok[0]['status'] < 2) {
		echo '
	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
	<form method="POST" action="index.php?pg=szemelyes/adataim" enctype="multipart/form-data">

		<div class="form-group">
			<label>Név</label>
			<input type="text" name="nev" class="form-control" value="'.$felhasznalok[0]['nev'].'">
		</div>
			<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="form-control" value="'.$felhasznalok[0]['email'].'">
		</div>
		<div class="form-group">
			<label>Telefonszám</label>
			<input type="text" name="tel" class="form-control" value="'.$felhasznalok[0]['tel'].'">
		</div>

		<div class="form-group">
			<label>Munkakör <br> <span style="color:red; font-size:15px;">(csak admin módosíthatja)</span></label>
			<input type="text" name="beosztas" class="form-control" readonly value="'.$felhasznalok[0]['beosztas'].'">
		</div>

		<div class="form-group">
			<label>Státusz <br> <span style="color:red; font-size:15px;">(csak admin módosíthatja)</span></label>
			<input type="text" name="status" class="form-control" readonly value="'.$felhasznalok[0]['status'].'">
		</div>

		<div class="form-group">
			<label>Profilkép</label><br>';

		if (empty($felhasznalok[0]['avatar']))
		{
		  echo '<input type="file" name="fileToUpload" id="fileToUpload"><br />';
		}
		else
		{
		  echo'<img src="images/avatar/'.$felhasznalok[0]['avatar'].'" class="img img-thumbnail"><br>
		  <a href="index.php?pg=szemelyes/avatardel" class="btn btn-danger">Profilkép törlése</a><br><br> ';
		}

		echo '
		</div>
		<div class="form-group">
			<input type="submit" name="modosit" class="btn btn-primary" value="Módosítás">
		</div>		
	</form>
	</div>
	<div class="col-xs-12 col-sm-3"></div>
	';


	}
	else
	{



		echo '
	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
	<form method="POST" action="index.php?pg=szemelyes/adataim" enctype="multipart/form-data">

		<div class="form-group">
			<label>Név</label>
			<input type="text" name="nev" class="form-control" value="'.$felhasznalok[0]['nev'].'">
		</div>
			<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="form-control" value="'.$felhasznalok[0]['email'].'">
		</div>
		<div class="form-group">
			<label>Telefonszám</label>
			<input type="text" name="tel" class="form-control" value="'.$felhasznalok[0]['tel'].'">
		</div>

		<div class="form-group">
			<label>Munkakör</label>
			<input type="text" name="beosztas" class="form-control" value="'.$felhasznalok[0]['beosztas'].'">
		</div>

		<div class="form-group">
			<label>Státusz</label>
			<input type="text" name="status" class="form-control" value="'.$felhasznalok[0]['status'].'">
		</div>

		<div class="form-group">
			<label>Profilkép</label><br>';

		if (empty($felhasznalok[0]['avatar']))
		{
		  echo '<input type="file" name="fileToUpload" id="fileToUpload"><br />';
		}
		else
		{
		  echo'<img src="images/avatar/'.$felhasznalok[0]['avatar'].'" class="img img-thumbnail"><br>
		  <a href="index.php?pg=szemelyes/avatardel" class="btn btn-danger">Profilkép törlése</a><br><br> ';
		}

		echo '
		</div>
		<div class="form-group">
			<input type="submit" name="modosit" class="btn btn-primary" value="Módosítás">
		</div>		
	</form>
	</div>
	<div class="col-xs-12 col-sm-3"></div>
	';
	}
?>