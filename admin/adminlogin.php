<?php

	//ha még nem létezik a munkamenet változó, ha még nem vagyunk belépve
	if (!isset($_SESSION['fid']))
	{

		//rákattintottunk-e a belépés gombra
		if (isset($_POST['belep']))
		{

			//SQL injection támadás elleni védekezés
			$email = escapeshellcmd($_POST['email']);
			$jelszo = escapeshellcmd($_POST['jelszo']);

			//ha az email vagy a jelszó mező üres, hibaüzenetet ad vissza
			if (empty($email) || empty($jelszo))
			{
				showError('Hiba! Nem adtál meg minden adatot!');
			}
			else
			{

				//email egyezés alapján lekérdezzük a felhasználót, ha nem ad vissza sort akkor a regisztráció még nem létezik
				$db->query("SELECT * FROM felhasznalok WHERE email='$email'");

				if ($db->numRows() == 0)
				{
					showError('Hiba! Nincs ilyen e-mail cím regisztrálva!');
				}
				else
				{
					//ha a megadott jelszó nem egyezik az adatbázisban lévővel szintén hibaüzenet
					$felhasznalo = $db->fetchAll();
					if (MD5($jelszo) != $felhasznalo[0]['jelszo'])
					{
						showError('Hiba! A megadott jelszó hibás!');
					} 
					else
					{

						//ha a felhasználó státusza 0 akkor a felhasználó tiltott
						if ($felhasznalo[0]['status'] == 0)
						{
							showError('Hiba! Tiltott felhasználó!');
						}
						else
						{
							$_SESSION['fid'] = $felhasznalo[0]['ID'];
							$_SESSION['fname'] = $felhasznalo[0]['nev'];
							$_SESSION['fmail'] = $felhasznalo[0]['email'];
							$_SESSION['fpic'] = $felhasznalo[0]['avatar'];
							//frissítjük a legutolsó belépés dátumát, idejét

							$db->query("UPDATE felhasznalok SET utbelepdatum=CURRENT_TIMESTAMP WHERE ID=".$_SESSION['fid']);
							header("location: index.php?pg=rendelesek/rendelesek");
						}
					}
				}
			}
		}
	echo '
			<form method="POST" action="index.php">
				<div class="form-group">
					<input type="text" name="email" placeholder="E-mail cím" class="form-control">
				</div>
				<div class="form-group">
					<input type="password" name="jelszo" placeholder="Jelszó" class="form-control">
				</div>
				<div class="form-group">
					<input type="submit" name="belep" value="Belépés" class="btn btn-warning c">
				</div>
			</form>';
			}
	else
	{
		// ha rákattintottunk a kilépés gombra
		if (isset($_POST['kilep']))
		{
			unset($_SESSION['fid']);
			unset($_SESSION['fmail']);
			unset($_SESSION['fname']);
			unset($_SESSION['fpic']);
			// újratöltjük az oldalt
			header("location: index.php");
		}

		echo '
		<h4>Szia, </h4>
		<h4>'.$_SESSION['fname'].'</h4>';

		if ($_SESSION['fpic'] == '')
		{
			echo '<a href="index.php?pg=szemelyes/szemelyes"><img title="PROFILOM" alt="PROFILOM" class="img-rounded img-responsive c img img-thumbnail avatar" src="images/avatar/nopic.jpg"></a>';
		}
		else
		{
			echo '<a href="index.php?pg=szemelyes/szemelyes"><img alt="PROFILOM" title="PROFILOM" class="img-rounded img-responsive c img img-thumbnail avatar" src="images/avatar/'.$_SESSION['fpic'].'"></a>';
		}

		echo'<br>
		<form method="POST" action="index.php">
			<div class="form-group">
				<input type="submit" name="kilep" value="Kilépés" class="btn btn-primary">
			</div>
		</form> ';

	}

?>