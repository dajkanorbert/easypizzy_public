<h3>Sajtok kezelése</h3>
<hr>

<!-- Új termék hozzáadása gomb, átírányít a felvétel oldalra -->
<a href="?pg=sajtok_felv" class="btn btn-primary">Új sajt hozzáadása</a>
<?php

	// kilistázza az összes terméket táblázatos formában
	$db->query("SELECT ID AS '@ID' , megnevezes AS 'Megnevezés' , leiras AS 'Leírás' , kcal AS 'Kcal' , ar AS 'Ár' FROM sajtok");
	$db->convertTable('i|u|d');
?>

<a href="?pg=menu/torzsadatok_kezelese" class="btn btn-primary">Vissza a törzsadatok kezeléséhez</a>