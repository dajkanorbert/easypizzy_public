<h3>Sajt törlése</h3>
<hr>
<?php

	//a törlendő termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintott a töröl gombra
	if (isset($_POST['torol']))
	{
		//kitöröljük az adatbázisból és visszairányítjuk a termék főoldalára
		$db->query("DELETE FROM sajtok WHERE ID=$id");
		header("location: index.php?pg=sajtok");
	}

	// ide fut először, hiszen az oldal betöltése után nem kattintottunk egyből a töröl gombra, biztonsági kérdés a termék törléséről
	echo 'Biztosan törlöd a következő sajtot?';

	$db->query("SELECT 
		megnevezes AS 'Megnevezés: ',
		kcal AS 'Kalória (kcal): ',
		ar AS 'Ár ($penznem)',
		leiras AS 'Leírás'
	 FROM sajtok WHERE ID=$id");

	$db->showRekord();

	echo '<form method="POST" action="index.php?pg=sajtok_del&id='.$id.'">
		<div class="form-group">
			<input type="submit" name="torol" value="Igen" class="btn btn-danger">
			<a href="?pg=sajtok" class="btn btn-primary">Mégsem</a>
		</div>
	</form>';
?>
