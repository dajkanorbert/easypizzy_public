<h3>Vásárló törlése</h3>
<hr>
<?php

	//a törlendő termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintott a töröl gombra
	if (isset($_POST['torol']))
	{
		//kitöröljük az adatbázisból és visszairányítjuk a termék főoldalára
		$db->query("DELETE FROM vasarlok WHERE ID=$id");
		header("location: index.php?pg=vasarlok/vasarlok_kezelese");
	}

	// ide fut először, hiszen az oldal betöltése után nem kattintottunk egyből a töröl gombra, biztonsági kérdés a termék törléséről
	echo 'Biztosan törlöd a következő vásárlót?';

	$db->query("SELECT ID , nev AS 'Név' , email AS 'E-mail cím' , tel AS 'Telefonszám' , regdatum AS 'Regisztrálás dátuma' , last AS 'Utolsó bejelentkezés' , vasalk AS 'Vásárlási alkalom' , vasosszeg AS 'Vásárlások összege' , penztarca AS 'EasyPizzy kreditek' , avatar AS 'Avatar' , status AS 'Státusz' FROM vasarlok WHERE ID=$id");

	$db->showRekord();

	echo '<form method="POST" action="index.php?pg=vasarlok_del&id='.$id.'">
		<div class="form-group">
			<input type="submit" name="torol" value="Törlés" class="btn btn-danger">
			<a href="?pg=vasarlok/vasarlok_kezelese" class="btn btn-primary">Mégsem</a>
		</div>
	</form>';
?>
