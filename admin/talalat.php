<h3>A keresés eredménye</h3>
<hr>
<?php
	$mit = $_POST['mit'];
	
	//
	// A pizzák között keresünk
	// 

$db->query("SELECT 
		pizzak.ID AS '@ID',
	 	pizzak.megnevezes AS 'Megnevezés',
	 	meretek.meret AS 'Méret',
		(meretek.ar*(tesztak.ar+szoszok.ar+husok.ar+sajtok.ar+zoldsegek.ar)) AS 'Ár ($penznem)'
		FROM pizzak
		INNER JOIN meretek ON pizzak.meret = meretek.ID
		INNER JOIN tesztak ON pizzak.tesztaID = tesztak.ID
		INNER JOIN szoszok ON pizzak.szoszID = szoszok.ID
		INNER JOIN husok ON pizzak.husID = husok.ID
		INNER JOIN sajtok ON pizzak.sajtID = sajtok.ID
		INNER JOIN zoldsegek ON pizzak.zoldsegID = zoldsegek.ID
		WHERE tesztak.megnevezes LIKE '%$mit%' OR tesztak.leiras LIKE '%$mit%' OR
			  szoszok.megnevezes LIKE '%$mit%' OR szoszok.leiras LIKE '%$mit%' OR
			  husok.megnevezes LIKE '%$mit%' OR husok.leiras LIKE '%$mit%' OR
			  sajtok.megnevezes LIKE '%$mit%' OR sajtok.leiras LIKE '%$mit%' OR
			  zoldsegek.megnevezes LIKE '%$mit%' OR zoldsegek.leiras LIKE '%$mit%'
			  ");

	if ($db->numRows() > 0)
	{
		echo '<h3>Találatok a pizzák között</h3>';
		$db->convertTable('i|u|d');
	}
	else
	{
		showInfo('A pizzák között nincs megfelelő találat!');
	}

	//
	// A dessertek között keresünk
	// 

	$db->query("SELECT * FROM desszertek WHERE leiras LIKE '%$mit%' OR megnevezes LIKE '%$mit%'");

	if ($db->numRows() > 0)
	{
		echo '<h3>Találatok a desszertek között</h3>';
		$db->convertTable('i|u|d');
	}
	else
	{
		showInfo('A desszertek között nincs megfelelő találat!');
	}

	//
	// Az üdítők között keresünk
	// 

	$db->query("SELECT * FROM uditok WHERE leiras LIKE '%$mit%' OR megnevezes LIKE '%$mit%'");

	if ($db->numRows() > 0)
	{
		echo '<h3>Találatok az üdítők között</h3>';
		$db->convertTable('i|u|d');
	}
	else
	{
		showInfo('Az üdítők között nincs megfelelő találat!');
	}	



?>