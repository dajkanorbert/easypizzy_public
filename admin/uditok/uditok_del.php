<h3>Üdítő törlése</h3>
<hr>
<?php

	//a törlendő termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintott a töröl gombra
	if (isset($_POST['torol']))
	{
		//kitöröljük az adatbázisból és visszairányítjuk a termék főoldalára
		$db->query("DELETE FROM uditok WHERE ID=$id");
		header("location: index.php?pg=uditok");
	}

	// ide fut először, hiszen az oldal betöltése után nem kattintottunk egyből a töröl gombra, biztonsági kérdés a termék törléséről
	echo 'Biztosan törlöd a következő üdítőt?';

	$db->query("SELECT 
		megnevezes AS 'Megnevezés: ',
		kcal AS 'Kalória (kcal): ',
		ar AS 'Ár',
		leiras AS 'Leírás', img AS 'Kép'
	 FROM uditok WHERE ID=$id");

	$db->showRekord();

	echo '<form method="POST" action="index.php?pg=uditok_del&id='.$id.'">
		<div class="form-group">
			<input type="submit" name="torol" value="Igen" class="btn btn-danger">
			<a href="?pg=uditok" class="btn btn-primary">Mégsem</a>
		</div>
	</form>';
?>
