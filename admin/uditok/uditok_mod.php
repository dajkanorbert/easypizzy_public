<h3>Üdítő módosítása</h3>
<hr>
<?php
	//a módosítandó termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintottunk már a módosít gombra akkor innen fut a program
	if (isset($_POST['modosit']))
	{
		//változóba mentjük a beírt adatokat és az escapeshellcmd-vel levédjük sql injection támadás ellen
		$megnevezes = escapeshellcmd($_POST['megnevezes']);
		$leiras = escapeshellcmd($_POST['leiras']);
		$kaloria = escapeshellcmd($_POST['kaloria']);
		$ar = escapeshellcmd($_POST['ar']);

		$avatar=$_FILES['fileToUpload']['name'];

		//ha üres valamelyik kötelezően kitöltendő mezőnk akkor írjon ki hibaszöveget
		if (empty($megnevezes) || empty($ar))
		{
			showError('Nem adtál meg minden adatot!');
		}
		else
		{

				//ha minden adat rendben van akkor módosítjuk a terméket és visszaírányítjuk a terméklistára
				$db->query("UPDATE uditok SET megnevezes='$megnevezes', leiras='$leiras', kcal=$kaloria, ar=$ar WHERE ID=$id");

				// Képfeltöltés
			    if(!empty($avatar))
			    {
			      uploadFile($_FILES['fileToUpload'],'target:../termekkepek/avatar|maxsize:2|allow:jpg,png,bmp,jpeg|filename:'.$avatar);
			      $db->query("UPDATE uditok SET img=

			      	'<a href=\"termekkepek/avatar/".$avatar."\" data-lightbox=\"X\"><img src=\"termekkepek/avatar/".$avatar."\" class=\"img-thumbnail img-rounded imghover img-responsive c img avatar\"></a>'

			      	WHERE ID=$id");
			    }

				header("location: index.php?pg=uditok");
		}
	}

	//lekérdezzük a módosítandó termék jelenlegi paramétereit és feltöltjük vele az űrlapot
	$db->query("SELECT * FROM uditok WHERE ID=$id");
	$uditok = $db->fetchAll();

	echo '
	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
	<form method="POST" action="index.php?pg=uditok_mod&id='.$id.'" enctype="multipart/form-data">
		<div class="form-group">
			<label>Megnevezés: *</label>
			<input type="text" name="megnevezes" class="form-control" value="'.$uditok[0]['megnevezes'].'">
		</div>
			<div class="form-group">
			<label>Leírás:</label>
			<textarea name="leiras" class="form-control">'.$uditok[0]['leiras'].'</textarea>
		</div>
		<div class="form-group">
			<label>Kalória:</label>
			<input type="number" name="kaloria" class="form-control" value="'.$uditok[0]['kcal'].'">
		</div>
		<div class="form-group">
			<label>Ár: *</label>
			<input type="number" name="ar" class="form-control" value="'.$uditok[0]['ar'].'">
		</div>

		<div class="form-group">
			<label>Termékkép</label><br>';

		if (empty($uditok[0]['img']))
		{
		  echo '<input type="file" name="fileToUpload" id="fileToUpload"><br />';
		}
		else
		{
		  print ($uditok[0]['img']);

		  echo '
		  	<br>
		  <a href="index.php?pg=uditok/avatardel&id='.$id.'" class="btn btn-danger">Termékkép törlése</a><br><br> ';
		}

		echo'
		<div class="form-group">
			<input type="submit" name="modosit" value="Módosít" class="btn btn-primary">
			<a href="?pg=uditok" class="btn btn-primary">Vissza az üdítők listájához</a>
			<br><br>
			A *-al jelölt adatok megadása köztelező!
		</div>
	</form>
	</div>
	<div class="col-xs-12 col-sm-3"></div>
	';
?>