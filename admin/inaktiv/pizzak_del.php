<h3>Pizza törlése</h3>
<hr>

<?php

	//a törlendő termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintott a töröl gombra
	if (isset($_POST['torol']))
	{
		//kitöröljük az adatbázisból és visszairányítjuk a termék főoldalára
		$db->query("DELETE FROM pizzak WHERE ID=$id");
		header("location: index.php?pg=pizzak");
	}

	// ide fut először, hiszen az oldal betöltése után nem kattintottunk egyből a töröl gombra, biztonsági kérdés a termék törléséről
	echo 'Biztosan törlöd az alábbi pizzát?';

	//lekérdezéssel kiírjuk a törlendő pizza adatait
	$db->query("SELECT 
		pizzak.ID AS 'Azonosító:',
	 	pizzak.megnevezes AS 'Megnevezés:',
	 	meretek.meret AS 'Méret (cm):',
	 	pizzak.rendeltdb AS 'Rendelt mennyiség (db):',
	 	tesztak.megnevezes AS 'Tészta:',
	 	szoszok.megnevezes AS 'Szósz:',
	 	husok.megnevezes AS 'Hús:',
	 	sajtok.megnevezes AS 'Sajt:',
	 	zoldsegek.megnevezes AS 'Zöldség:',
	 	(meretek.ar*(tesztak.ar+szoszok.ar+husok.ar+sajtok.ar+zoldsegek.ar)) AS 'Ára ($penznem):',
	 	(tesztak.kcal+szoszok.kcal+husok.kcal+sajtok.kcal+zoldsegek.kcal) AS 'Kalória (kcal):'
		FROM pizzak
		INNER JOIN meretek ON pizzak.meret = meretek.ID
		INNER JOIN tesztak ON pizzak.tesztaID = tesztak.ID
		INNER JOIN szoszok ON pizzak.szoszID = szoszok.ID
		INNER JOIN husok ON pizzak.husID = husok.ID
		INNER JOIN sajtok ON pizzak.sajtID = sajtok.ID
		INNER JOIN zoldsegek ON pizzak.zoldsegID = zoldsegek.ID

		WHERE pizzak.ID=$id");

		$db->showRekord();

		echo '<form method="POST" action="index.php?pg=pizzak_del&id='.$id.'">
			<div class="form-group">
				<input type="submit" name="torol" value="Igen, törlöm" class="btn btn-danger">
				<a href="index.php?pg=pizzak" class="btn btn-primary">Vissza a pizzák listájára</a>
			</div>	  
		</form>';
?>

