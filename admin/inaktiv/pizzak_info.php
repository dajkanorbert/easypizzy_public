<h3>Pizza információ</h3>
<hr>
<?php
	$id = $_GET['id'];

	$db->query("SELECT 
		pizzak.ID AS 'Azonosító:',
	 	pizzak.megnevezes AS 'Megnevezés:',
	 	meretek.meret AS 'Méret (cm):',
	 	pizzak.rendeltdb AS 'Rendelt mennyiség (db):',
	 	tesztak.megnevezes AS 'Tészta:',
	 	szoszok.megnevezes AS 'Szósz:',
	 	husok.megnevezes AS 'Hús:',
	 	sajtok.megnevezes AS 'Sajt:',
	 	zoldsegek.megnevezes AS 'Zöldség:',
	 	(meretek.ar*(tesztak.ar+szoszok.ar+husok.ar+sajtok.ar+zoldsegek.ar)) AS 'Ára ($penznem):',
	 	(tesztak.kcal+szoszok.kcal+husok.kcal+sajtok.kcal+zoldsegek.kcal) AS 'Kalória (kcal):'
		FROM pizzak
		INNER JOIN meretek ON pizzak.meret = meretek.ID
		INNER JOIN tesztak ON pizzak.tesztaID = tesztak.ID
		INNER JOIN szoszok ON pizzak.szoszID = szoszok.ID
		INNER JOIN husok ON pizzak.husID = husok.ID
		INNER JOIN sajtok ON pizzak.sajtID = sajtok.ID
		INNER JOIN zoldsegek ON pizzak.zoldsegID = zoldsegek.ID

		WHERE pizzak.ID=$id");

	$db->showRekord();
/*
	$db->query("SELECT 
		(meretek.ar*(tesztak.ar+szoszok.ar+husok.ar+sajtok.ar+zoldsegek.ar)) AS 'osszar'
		FROM pizzak
		INNER JOIN meretek ON pizzak.meret = meretek.ID
		INNER JOIN tesztak ON pizzak.tesztaID = tesztak.ID
		INNER JOIN szoszok ON pizzak.szoszID = szoszok.ID
		INNER JOIN husok ON pizzak.husID = husok.ID
		INNER JOIN sajtok ON pizzak.sajtID = sajtok.ID
		INNER JOIN zoldsegek ON pizzak.zoldsegID = zoldsegek.ID

		WHERE pizzak.ID=$id");
		$res = $db->fetchAll();
		echo 'A pizza ára: <b>'.szamkiir($res[0]['osszar']).'</b> '.$penznem.'<br><br>';
		*/
?>

	<a href="?pg=pizzak" class="btn btn-primary">Vissza a pizzák listájára</a>