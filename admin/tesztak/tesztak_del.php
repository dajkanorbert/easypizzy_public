<h3>Tészta törlése</h3>
<hr>
<?php

	//a törlendő termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintott a töröl gombra
	if (isset($_POST['torol']))
	{
		//kitöröljük az adatbázisból és visszairányítjuk a termék főoldalára
		$db->query("DELETE FROM tesztak WHERE ID=$id");
		header("location: index.php?pg=tesztak");
	}
	
	// ide fut először, hiszen az oldal betöltése után nem kattintottunk egyből a töröl gombra, biztonsági kérdés a termék törléséről
	echo 'Biztosan törlöd a következő tésztát?';

	$db->query("SELECT 
		megnevezes AS 'Megnevezés: ',
		kcal AS 'Kalória (kcal): ',
		ar AS 'Ár ($penznem)',
		leiras AS 'Leírás'
	 FROM tesztak WHERE ID=$id");

	$db->showRekord();

	echo '<form method="POST" action="index.php?pg=tesztak_del&id='.$id.'">
		<div class="form-group">
			<input type="submit" name="torol" value="Törlés" class="btn btn-danger">
			<a href="?pg=tesztak" class="btn btn-primary">Mégsem</a>
		</div>
	</form>';
?>
