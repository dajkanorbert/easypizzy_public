<h3>Tészták kezelése</h3>
<hr>

<!-- Új termék hozzáadása gomb, átírányít a felvétel oldalra -->
<a href="?pg=tesztak_felv" class="btn btn-primary">Új tészta hozzáadása</a>
<?php

	// kilistázza az összes húst táblázatos formában
	$db->query("SELECT ID AS '@ID' , megnevezes AS 'Megnevezés' , leiras AS 'Leírás' , kcal AS 'Kcal' , ar AS 'ÁR' FROM tesztak");
	$db->convertTable('i|u|d');
?>

<a href="?pg=menu/torzsadatok_kezelese" class="btn btn-primary">Vissza a törzsadatok kezeléséhez</a>