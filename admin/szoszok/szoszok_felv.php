<h3>Új szósz hozzáadása</h3>
<hr>
<?php
	//ellenőrzi, hogy rákattintottunk-e a felvesz gombra
	if (isset($_POST['felvesz']))
	{
		//változóba mentjük a beírt adatokat és az escapeshellcmd-vel levédjük sql injection támadás ellen
		$megnevezes = escapeshellcmd($_POST['megnevezes']);
		$leiras = escapeshellcmd($_POST['leiras']);
		$kaloria = escapeshellcmd($_POST['kaloria']);
		$ar = escapeshellcmd($_POST['ar']);

		//ha üres valamelyik kötelezően kitöltendő mezőnk akkor írjon ki hibaszöveget
		if (empty($megnevezes) || empty($ar))
		{
			showError('Nem adtál meg minden adatot!');
		}
		else
		{
			//lekérdezzük megnevezés alapján és, ha 0-nál több találatot ad vissza akkor a termék már létezik az adatbázisban
			$db->query("SELECT ID FROM szoszok WHERE megnevezes='$megnevezes'");
			if ($db->numRows() != 0)
			{
				showError('Van már ilyen nevű szósz az adatbázisban!');
			}
			else
			{
				//ha minden adat rendben van akkor beszúrjuk a táblába a terméket és visszaírányítjuk a termékek "főoldalára"
				$db->query("INSERT INTO szoszok VALUES(null, '$megnevezes', '$leiras', $kaloria, $ar)");
				header("location: index.php?pg=szoszok");
			}
		}

	}
?>

	<!-- Űrlap új adat felvételéhez -->
<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
		<form method="POST" action="?pg=szoszok_felv">
			<div class="form-group">
				<label>Megnevezés: *</label>
				<input type="text" name="megnevezes" class="form-control">
			</div>
				<div class="form-group">
				<label>Leírás:</label>
				<textarea name="leiras" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<label>Kalória:</label>
				<input type="number" name="kaloria" class="form-control">
			</div>
			<div class="form-group">
				<label>Ár: *</label>
				<input type="number" name="ar" class="form-control">
			</div>
			<div class="form-group">
				<input type="submit" name="felvesz" value="Felvesz" class="btn btn-primary">
				<a href="?pg=szoszok" class="btn btn-primary">Vissza a szószok listájához</a>
				<br><br>
				A *-al jelölt adatok megadása kötelező!
			</div>
		</form>
	</div>
<div class="col-xs-12 col-sm-3"></div>