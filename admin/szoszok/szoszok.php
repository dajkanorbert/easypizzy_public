<h3>Szószok kezelése</h3>
<hr>
<a href="?pg=szoszok_felv" class="btn btn-primary">Új szósz hozzáadása</a>
<!-- Új termék hozzáadása gomb, átírányít a felvétel oldalra -->
<?php

	// kilistázza az összes húst táblázatos formában
	$db->query("SELECT ID AS '@ID' , megnevezes AS 'Megnevezés' , leiras AS 'Leírás' , kcal AS 'Kcal' , ar AS 'Ár' FROM szoszok");
	$db->convertTable('i|u|d');
?>

<a href="?pg=menu/torzsadatok_kezelese" class="btn btn-primary">Vissza a törzsadatok kezeléséhez</a>