<!DOCTYPE html>
<?php
// elindítjuk a php-t, includeoljuk az induláshoz nélkülözhetetlen fájlokat
	session_start();
	ob_start();
	require("../adatok.php");
	require("../fuggvenytar.php");
	require("../database.php");

	// itt hozzuk létre a saját adatbázis objektumunkat $db néven
	$db = new db($dbhost, $dbuser, $dbpass, $dbname);
?>

<html>
<head>
	<!-- utf8-ra állítjuk a karakterkódolást-->
	<meta charset="utf-8">

	<!-- változóból íratjuk ki az oldal címét-->
	<title><?php echo $title; ?></title>

	<!-- saját ikont importálunk a címsorba -->
	<link rel="shortcut icon" href="../images/favicon.ico">

	<!-- beimportáljuk a css fájlokat -->
	<link rel="stylesheet" type="text/css" href="../css/lightbox.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../css/menuteszt.css">
	<link rel="stylesheet" type="text/css" href="../css/image.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">

	<!-- beimportáljuk a javascript fájlokat -->
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/jquery.canvasjs.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script src="../js/6e3ea7b307.js" crossorigin="anonymous"></script>

	<!-- beimportáljuk a naptárat -->
	<link href='../fullcalendar/core/main.css' rel='stylesheet' />
	<link href='../fullcalendar/daygrid/main.css' rel='stylesheet' />
	<link href='../fullcalendar/timegrid/main.css' rel='stylesheet' />
	<link href='../fullcalendar/list/main.css' rel='stylesheet' />

	<script src='../fullcalendar/core/main.js'></script>
  	<script src='../fullcalendar/core/locales/hu.js'></script>
	<script src='../fullcalendar/interaction/main.js'></script>
	<script src='../fullcalendar/daygrid/main.js'></script>
	<script src='../fullcalendar/timegrid/main.js'></script>
  	<script src='../fullcalendar/list/main.js'></script> 
</head>
<body>
	<!-- az oldal lényegi tartalma, a teljes szélességtől kissebb méreten -->

	<div class="container" id="container">
		<div class="col-md-9 hidden-xs hidden-sm"></div>

			<!-- keresőmező -->
	<div class="col-xs-12 col-md-3 search c">
	<?php
	if (isset($_SESSION['fid']))
		{
			include("kereses.php"); 
		}
	 ?>
	<br>
	</div>

		<!-- Logó importálása főoldalra mutató linkkel -->
		<header class="col-xs-12">
		<div id="logo" class="col-xs-12 col-md-3">
				<a href="index.php"><img class="logo" src="../images/logo2.png" alt="EasyPizzy"></a>
			</div>

			<!-- Nyujtófa importálása középre -->
			<div id="row" class="col-xs-12 col-md-6 c" ><img alt="Admin logó" src="../images/admin.png"></div>

			<!-- Login mező -->
			<div id="login" class="col-xs-12 col-md-3">
				<?php include("adminlogin.php"); ?>
			</div>
		</header>

		<!-- oldal törzse -->
		<section class="col-xs-12 c">

			<!-- Menü -->
			<article id="menu" class="col-xs-12 c">
				 <?php
				 if (isset($_SESSION['fid']))
				 {
				 	include("menuadmin.php");
				 }
				   ?>
			</article>

			<!-- Betöltő, ide töltődnek be a közvetetten linkelt oldalak-->
			<article id="content" class="col-xs-12 c">
				 <?php
				 if (isset($_SESSION['fid']))
				 {
				 	include("loader.php");
				 }
				 else 
				 {
				 	showError("Az oldal csak admin jogosultsággal tekinthető meg!");
				 	echo '

				 	<p class="c">
					<b>Alapértelmezett admin felhasználó:</b><br>
					E-mail cím: <b>admin</b><br>
					Jelszó: <b>admin</b>
					</p>
	
				 	<a href="../index.php">
				 	<span class="black">Vissza az EasyPizzy főoldalára</span>
				 	</a>';
				 }
				   ?>
			</article>
		</section>

		<!-- Lábléc -->
		<footer class="col-xs-12">
			<?php echo $company.' - '.$author.' - '.$title; ?>
			<span class="glyphicon glyphicon-registration-mark "></span> <br>
			<span class="glyphicon glyphicon-warning-sign"></span>
			<a href="#">ÁSZF</a> - 
			<a href="#">Adatvédelem</a> - 
			<a href="#">Impresszum</a> 
			<span class="glyphicon glyphicon-warning-sign"></span>
			<br>
			<!-- pizzaszelet ikon -->
			<div id="pizzaikon">
				<i class="fas fa-pizza-slice"></i>
			</div>

		</footer>
	</div>
	<?php ob_end_flush(); ?>
	<script type="text/javascript" src="../js/lightbox.js"></script>
</body>
</html>