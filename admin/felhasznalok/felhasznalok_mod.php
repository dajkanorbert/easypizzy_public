<h3>Felhasználó módosítása</h3>
<hr>
<?php
	//a módosítandó termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintottunk már a módosít gombra akkor innen fut a program
	if (isset($_POST['modosit']))
	{
		//változóba mentjük a beírt adatokat és az escapeshellcmd-vel levédjük sql injection támadás ellen
		$nev = escapeshellcmd($_POST['nev']);
		$email = escapeshellcmd($_POST['email']);
		$tel = escapeshellcmd($_POST['tel']);
		$beosztas = escapeshellcmd($_POST['beosztas']);
		$status = escapeshellcmd($_POST['status']);


		//ha üres valamelyik kötelezően kitöltendő mezőnk akkor írjon ki hibaszöveget
		if (empty($nev) || empty($email) || empty($tel) || empty($beosztas) || empty($status))
		{
			showError('Nem adtál meg minden adatot!');
		}
		else
		{

				//ha minden adat rendben van akkor módosítjuk a terméket és visszaírányítjuk a terméklistára
				$db->query("UPDATE felhasznalok SET nev='$nev', email='email', tel='$tel', beosztas='$beosztas' , status='$status' WHERE ID=$id");


				header("location: index.php?pg=felhasznalok/felhasznalok_kezeles");
		}
	}

	//lekérdezzük a módosítandó termék jelenlegi paramétereit és feltöltjük vele az űrlapot
	$db->query("SELECT * FROM felhasznalok WHERE ID=$id");
	$felhasznalok = $db->fetchAll();

	echo '
	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
	<form method="POST" action="index.php?pg=felhasznalok_mod&id='.$id.'">

		<div class="form-group">
			<label>Név</label>
			<input type="text" name="nev" class="form-control" value="'.$felhasznalok[0]['nev'].'">
		</div>
			<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="form-control" value="'.$felhasznalok[0]['email'].'">
		</div>
		<div class="form-group">
			<label>Telefonszám</label>
			<input type="text" name="tel" class="form-control" value="'.$felhasznalok[0]['tel'].'">
		</div>

		<div class="form-group">
			<label>Munkakör <br> <span style="color:red; font-size:15px;">(csak admin módosíthatja)</span></label>
			<input type="text" name="beosztas" class="form-control" value="'.$felhasznalok[0]['beosztas'].'">
		</div>

		<div class="form-group">
			<label>Státusz <br> <span style="color:red; font-size:15px;">(csak admin módosíthatja)</span></label>
			<input type="text" name="status" class="form-control" value="'.$felhasznalok[0]['status'].'">
		</div>
				<div class="form-group">
			<input type="submit" name="modosit" class="btn btn-primary" value="Módosítás">
	</form>
	</div>
	<div class="col-xs-12 col-sm-3"></div>
	';
?>