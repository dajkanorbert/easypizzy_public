<h3>Felhasználó törlése</h3>
<hr>
<?php
	ob_start();

	//a törlendő felhasználó ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintott a töröl gombra
	if (isset($_POST['torol']))
	{

		//kitöröljük az adatbázisból és visszairányítjuk a desszertekhez
		$db->query("DELETE FROM felhasznalok WHERE ID=$id");
		showSuccess('Az alkalmazott törölve lett az adatbázisból!');
		header("location:index.php?pg=felhasznalok/felhasznalok_kezeles");
	}

	// ide fut először, hiszen az oldal betöltése után nem kattintottunk egyből a töröl gombra, biztonsági kérdés a termék törléséről
	echo 'Biztosan törlöd a következő alkalmazottat?';

	$db->query("SELECT ID , nev AS 'Név' , email AS 'E-mail' , tel AS 'Telefonszám' , beosztas AS 'Beosztás' , pontszam AS 'Pontszám' , status AS 'Státusz' , regdatum AS 'Regisztrálás dáuma' , utbelepdatum AS 'Utolsó belépés' FROM felhasznalok WHERE ID=$id");

	$db->showRekord();

	echo '<form method="POST" action="index.php?pg=felhasznalok_del&id='.$id.'">
		<div class="form-group">
			<input type="submit" name="torol" value="TÖRLÉS" class="btn btn-danger">
			<a href="index.php?pg=felhasznalok/felhasznalok_kezeles" class="btn btn-primary">Mégsem</a>
		</div>
	</form>';
	ob_end_flush();
?>
