<h3>Alkalmazott regisztrálása</h3>
<hr>


<?php
	if (isset($_POST['regisztracio']))
	{
		$nev = escapeshellcmd($_POST['nev']);
		$phone = escapeshellcmd($_POST['phone']);
		$email = escapeshellcmd($_POST['email']);
		$email2 = escapeshellcmd($_POST['email2']);
		$pass1 = escapeshellcmd($_POST['pass1']);
		$pass2 = escapeshellcmd($_POST['pass2']);
		$beosztas = escapeshellcmd($_POST['beosztas']);
		$status = escapeshellcmd($_POST['status']);

		if (empty($nev) || empty($email) || empty($email2) || empty($pass1) || empty($pass2) || empty($status) || empty($phone))
		{
			showError('Hiba! Nem adtál meg minden adatot!');
		}
		else
		{
			if ($pass1 != $pass2)
			{
				showError('Hiba! A megadott jelszavak nem egyeznek!');
			}
			else
			{
				$db->query("SELECT * FROM felhasznalok WHERE email='$email'");
				if ($db->numRows() != 0)
				{
					showError('Hiba! Ez az e-mail cím már használatban van!');
				}
				else
				{
					if(!preg_match('/^[0-9A-Za-z!@#$%]{8,12}$/', $pass1))
					{
    					showError("A megadott jelszó nem felel meg a biztonsági kritériumoknak!");
					}
					else
					 {
					$pass1 = MD5($pass1);
					$db->query("INSERT INTO felhasznalok VALUES(null, '$nev', 
					'$email', '$pass1' , '$phone', '$beosztas' , 0 , '$status' , '' , CURRENT_TIMESTAMP , CURRENT_TIMESTAMP)");
					showSuccess('A regisztráció sikeres!');
					}
				}
			}
		}
	}
?>


<div class="col-xs-12 col-sm-2"></div>
<form method="POST" action="index.php?pg=felhasznalok/felhasznalok_reg">
	<div class="c col-xs-12 col-sm-8">
		<div class="form-group">
			<label for="nev">Teljes név:</label>
			<input type="text" name="nev" class="form-control">
		</div>
		<div class="form-group">
			<label for="phone">Telefonszám:</label>
			<input type="tel" name="phone" class="form-control">
		</div>
		<div class="form-group">
			<label for="email">E-mail cím:</label>
			<input type="email" name="email" class="form-control">
		</div>
		<div class="form-group">
			<label for="email2">E-mail cím mégegyszer:</label>
			<input type="email" name="email2" class="form-control">
		</div>
		<div class="form-group">
			<label for="status">Alkalmazott státusza: (0-inaktív | 1-alkalmazott | 2-admin)</label>
			<input type="text" name="status" class="form-control">
		</div>
		<div class="form-group">
			<label for="beosztas">Alkalmazott beosztása: (futar vagy szakacs)</label>
			<input type="text" name="beosztas" class="form-control">
		</div>
		<div class="form-group">
			<label for="pass1">Jelszó:</label>
			<input type="password" name="pass1" class="form-control">
		</div>
		<div class="form-group">
			<label for="pass2">Jelszó megerősítése:</label>
			<input type="password" name="pass2" class="form-control">
		</div>
		<div class="form-group">
			<input type="submit" name="regisztracio" value="Regisztráció" class="btn btn-primary">
		</div>	
	</div>
	<div class="col-xs-12 col-sm-2"></div>
</form>