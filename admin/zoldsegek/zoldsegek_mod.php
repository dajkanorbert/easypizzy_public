<h3>Zöldségek módosítása</h3>
<hr>
<?php
	//a módosítandó termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintottunk már a módosít gombra akkor innen fut a program
	if (isset($_POST['modosit']))
	{
		//változóba mentjük a beírt adatokat és az escapeshellcmd-vel levédjük sql injection támadás ellen
		$megnevezes = escapeshellcmd($_POST['megnevezes']);
		$leiras = escapeshellcmd($_POST['leiras']);
		$kaloria = escapeshellcmd($_POST['kaloria']);
		$ar = escapeshellcmd($_POST['ar']);

		//ha üres valamelyik kötelezően kitöltendő mezőnk akkor írjon ki hibaszöveget
		if (empty($megnevezes) || empty($ar))
		{
			showError('Nem adtál meg minden adatot!');
		}
		else
		{
			//lekérdezzük megnevezés alapján és, ha 0-nál több találatot ad vissza akkor a termék már létezik az adatbázisban
			$db->query("SELECT ID FROM zoldsegek WHERE megnevezes='$megnevezes' AND ID<>$id");
			if ($db->numRows() != 0)
			{
				showError('Van már ilyen nevű zöldség az adatbázisban!');
			}
			else
			{
				//ha minden adat rendben van akkor módosítjuk a terméket és visszaírányítjuk a terméklistára
				$db->query("UPDATE zoldsegek SET megnevezes='$megnevezes', leiras='$leiras', kcal=$kaloria, ar=$ar WHERE ID=$id");
				header("location: index.php?pg=zoldsegek");
			}
		}
	}

	//lekérdezzük a módosítandó termék jelenlegi paramétereit és feltöltjük vele az űrlapot
	$db->query("SELECT * FROM zoldsegek WHERE ID=$id");
	$zoldsegek = $db->fetchAll();

	echo '
	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
	<form method="POST" action="index.php?pg=zoldsegek_mod&id='.$id.'">
		<div class="form-group">
			<label>Megnevezés: *</label>
			<input type="text" name="megnevezes" class="form-control" value="'.$zoldsegek[0]['megnevezes'].'">
		</div>
			<div class="form-group">
			<label>Leírás:</label>
			<textarea name="leiras" class="form-control">'.$zoldsegek[0]['leiras'].'</textarea>
		</div>
		<div class="form-group">
			<label>Kalória:</label>
			<input type="number" name="kaloria" class="form-control" value="'.$zoldsegek[0]['kcal'].'">
		</div>
		<div class="form-group">
			<label>Ár: *</label>
			<input type="number" name="ar" class="form-control" value="'.$zoldsegek[0]['ar'].'">
		</div>
		<div class="form-group">
			<input type="submit" name="modosit" value="Módosít" class="btn btn-primary">
			<a href="?pg=zoldsegek" class="btn btn-primary">Vissza a zöldségek listájához</a>
			<br><br>
			A *-al jelölt adatok megadása köztelező!
		</div>
	</form>
	</div>
	<div class="col-xs-12 col-sm-3"></div>
	';
?>