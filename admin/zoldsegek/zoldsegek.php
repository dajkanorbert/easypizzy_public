<h3>Zöldségek kezelése</h3>
<hr>

<!-- Új termék hozzáadása gomb, átírányít a felvétel oldalra -->
<a href="?pg=zoldsegek_felv" class="btn btn-primary">Új zöldség hozzáadása</a>
<?php

	// kilistázza az összes terméket táblázatos formában
	$db->query("SELECT ID AS '@ID' , megnevezes AS 'Megnevezés' , leiras AS 'Leírás' , kcal AS 'Kcal' , ar AS 'Ár' FROM zoldsegek");
	$db->convertTable('i|u|d');
?>

<a href="?pg=menu/torzsadatok_kezelese" class="btn btn-primary">Vissza a törzsadatok kezeléséhez</a>