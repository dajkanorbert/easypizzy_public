<h3>Hús alapanyag törlése</h3>
<hr>
<?php

	//a törlendő termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintott a töröl gombra
	if (isset($_POST['torol']))
	{
		//kitöröljük az adatbázisból és visszairányítjuk a desszertekhez
		$db->query("DELETE FROM husok WHERE ID=$id");
		header("location: index.php?pg=husok");
	}

	// ide fut először, hiszen az oldal betöltése után nem kattintottunk egyből a töröl gombra, biztonsági kérdés a termék törléséről
	echo 'Biztosan törlöd a következő hús alapanyagot?';

	$db->query("SELECT 
		megnevezes AS 'Megnevezés: ',
		kcal AS 'Kalória (kcal): ',
		ar AS 'Ár ($penznem)',
		leiras AS 'Leírás'
	 FROM husok WHERE ID=$id");

	$db->showRekord();

	echo '<form method="POST" action="index.php?pg=husok_del&id='.$id.'">
		<div class="form-group">
			<input type="submit" name="torol" value="Törlés" class="btn btn-danger">
			<a href="?pg=husok" class="btn btn-primary">Mégsem</a>
		</div>
	</form>';
?>
