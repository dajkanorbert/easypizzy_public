<h3>Húsok kezelése</h3>
<hr>

<!-- Új hús hozzáadása gomb, átírányít felvétel oldalra -->
<a href="?pg=husok_felv" class="btn btn-primary">Új hús alapanyag hozzáadása</a>
<?php

	// kilistázza az összes húst táblázatos formában
	$db->query("SELECT ID AS '@ID' , megnevezes AS 'Megnevezés' , leiras AS 'Leírás' , kcal AS 'Kcal' , ar AS 'Ár' FROM husok");
	$db->convertTable('i|u|d');
?>

<a href="?pg=menu/torzsadatok_kezelese" class="btn btn-primary">Vissza a törzsadatok kezeléséhez</a>