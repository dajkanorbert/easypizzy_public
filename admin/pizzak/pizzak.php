<h3>Pizzák kezelése</h3>
<hr>

<!-- Új termék hozzáadása gomb, átírányít a felvétel oldalra -->
<a href="?pg=pizzak_felv" class="btn btn-primary">Új pizza hozzáadása</a>
<?php

	//kilistázza az összes terméket táblázatos formában
	$db->query("SELECT ID,megnevezes AS 'Megnevezés', meret AS 'Méret',teszta AS 'Tészta',ar AS 'Ár',kcal AS 'Kcal',kep AS 'Kép' FROM attekinto");
	/* $db->query("SELECT 
		pizzak.ID AS '@ID',
	 	pizzak.megnevezes AS 'Megnevezés',
	 	meretek.meret AS 'Méret',
		(meretek.ar*(tesztak.ar+szoszok.ar+husok.ar+sajtok.ar+zoldsegek.ar)) AS 'Ár ($penznem)'
		FROM pizzak
		INNER JOIN meretek ON pizzak.meret = meretek.ID
		INNER JOIN tesztak ON pizzak.tesztaID = tesztak.ID
		INNER JOIN szoszok ON pizzak.szoszID = szoszok.ID
		INNER JOIN husok ON pizzak.husID = husok.ID
		INNER JOIN sajtok ON pizzak.sajtID = sajtok.ID
		INNER JOIN zoldsegek ON pizzak.zoldsegID = zoldsegek.ID
		"); */
	$db->convertTable('i|u|d');
?>

<a href="?pg=menu/torzsadatok_kezelese" class="btn btn-primary">Vissza a törzsadatok kezeléséhez</a>

