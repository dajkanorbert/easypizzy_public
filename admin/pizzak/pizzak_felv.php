<h3>Új pizza felvétele</h3>
<hr>
<?php
	//ellenőrzi, hogy rákattintottunk-e a felvesz gombra
	if (isset($_POST['felvesz']))
	{
		//változóba mentjük a beírt adatokat és az escapeshellcmd-vel levédjük sql injection támadás ellen
		$megnevezes = escapeshellcmd($_POST['pizzanev']);
		$meretID = $_POST['meretek'];
		$tesztaID = $_POST['tesztak'];
		$szoszID = $_POST['szoszok'];
		$sajtID = $_POST['sajtok'];
		$husID = $_POST['husok'];
		$zoldsegID = $_POST['zoldsegek'];

		$avatar=$_FILES['fileToUpload']['name'];

		//ha üres valamelyik kötelezően kitöltendő mezőnk akkor írjon ki hibaszöveget
		if (empty($megnevezes) || empty($meretID) || empty($tesztaID))
		{
			showError("Nem adtál meg minden adatot!");
		}
		else
		{
			//lekérdezzük megnevezés alapján és, ha 0-nál több találatot ad vissza akkor a termék már létezik az adatbázisban
			$db->query("SELECT ID FROM pizzak WHERE megnevezes = '$megnevezes'");
			if ($db->numRows() != 0)
			{
				showError("Van már ilyen nevű pizza!");
			}
			else
			{
				//ha minden adat rendben van akkor beszúrjuk a táblába a terméket és visszaírányítjuk a termékek "főoldalára"
				$db->query("INSERT INTO pizzak VALUES(null, '$megnevezes',$meretID,$tesztaID,$szoszID,$husID,$zoldsegID,$sajtID,1,'',0)");

				// Képfeltöltés
			    if(!empty($avatar))
			    {
			      uploadFile($_FILES['fileToUpload'],'target:../termekkepek/avatar|maxsize:2|allow:jpg,png,bmp,jpeg|filename:'.$avatar);
			      $id = $db->LastID();
			      $db->query("UPDATE pizzak SET img=
			      	'<a href=\"termekkepek/avatar/".$avatar."\" data-lightbox=\"X\"><img src=\"termekkepek/avatar/".$avatar."\" class=\"img-thumbnail img-rounded imghover img-responsive c img avatar\"></a>' WHERE ID=$id");

			    }

				header("location: index.php?pg=pizzak");
			}
		}

	}

	// Űrlap új adat felvételéhez selectes kiválasztással
	echo '
	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
	
	<form method="POST" action="index.php?pg=pizzak_felv" enctype="multipart/form-data">
		<div class="form-group">
			<label>Megnevezés:</label>
			<input type="text" name="pizzanev" class="form-control">
		</div>
		<div class="form-group">
			<label>Méret:</label>';
			$db->query("SELECT * FROM meretek");
			$db->convertSelect('ID', 'meret', '');
			echo '
		</div>
		<div class="form-group">
			<label>Tészta:</label>';
			$db->query("SELECT * FROM tesztak");
			$db->convertSelect('ID', 'megnevezes', '');
			echo '
		</div>
		<div class="form-group">
			<label>Szósz:</label>';
			$db->query("SELECT * FROM szoszok");
			$db->convertSelect('ID', 'megnevezes', '');
			echo '
		</div>
		<div class="form-group">
			<label>Hús:</label>';
			$db->query("SELECT * FROM husok");
			$db->convertSelect('ID', 'megnevezes', '');
			echo '
		</div>
		<div class="form-group">
			<label>Sajt:</label>';
			$db->query("SELECT * FROM sajtok");
			$db->convertSelect('ID', 'megnevezes', '');
			echo '
		</div>
		<div class="form-group">
			<label>Zöldség:</label>';
			$db->query("SELECT * FROM zoldsegek");
			$db->convertSelect('ID', 'megnevezes', '');
			echo '
		</div>

			<div class="form-group">
			<label>Termékkép</label><br>';

		if (empty($avatar))
		{
		  echo '<input type="file" name="fileToUpload" id="fileToUpload"><br />';
		}
		else
		{
		  echo'<img src="termekkepek/avatar/'.$avatar.'" class="img img-thumbnail"><br>
		  <a href="index.php?pg=pizzak/avatardel" class="btn btn-danger">Termékkép törlése</a><br><br> ';
		}

		echo'
		<div class="form-group">
			<input type="submit" name="felvesz" value="Felvesz" class="btn btn-primary">
			<a href="index.php?pg=pizzak" class="btn btn-primary">Vissza a pizzák listájára</a>
		</div>
	</form>
	</div>
<div class="col-xs-12 col-sm-3"></div>
	';

?>