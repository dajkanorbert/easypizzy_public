<h3>Pizza törlése</h3>
<hr>

<?php

	//a törlendő termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintott a töröl gombra
	if (isset($_POST['torol']))
	{
		//kitöröljük az adatbázisból és visszairányítjuk a termék főoldalára
		$db->query("DELETE FROM pizzak WHERE ID=$id");
		header("location: index.php?pg=pizzak");
	}

	// ide fut először, hiszen az oldal betöltése után nem kattintottunk egyből a töröl gombra, biztonsági kérdés a termék törléséről
	echo 'Biztosan törlöd az alábbi pizzát?';

	//lekérdezéssel kiírjuk a törlendő pizza adatait
	$db->query("SELECT * FROM attekinto WHERE ID=$id");

		$db->showRekord();

		echo '<form method="POST" action="index.php?pg=pizzak_del&id='.$id.'">
			<div class="form-group">
				<input type="submit" name="torol" value="Igen, törlöm" class="btn btn-danger">
				<a href="index.php?pg=pizzak" class="btn btn-primary">Vissza a pizzák listájára</a>
			</div>	  
		</form>';
?>

