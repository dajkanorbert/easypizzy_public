<h3>Rendelés Statisztika</h3>
<hr>
<?php

	//lekérdezzük a rendelések végösszegét és dátum szerint csoportosítjuk
	$db->query("SELECT datum AS 'Dátum', SUM(vegosszeg) as 'Összesen' FROM v_rendelesek WHERE rendeles_statusz=0 GROUP BY datum");
	$db->convertTable('');

	//a kapott eredményeket táblázatos és grafikon formában kiírjuk a már előre definált, jQuery segítségével létrehozott convertChart metódussal
	$db->convertChart('line', 'Rendelések', 'Dátum', 'Összesen', 'chart', 'light1', true);
?>

	<!-- Chart ID-vel létrehozott divet a css-ben formázzuk, hogy a grafikon jól illeszkedjen az oldalunkhoz -->
<div id="chart"></div><br>

<?php
	echo'<a href="?pg=menu/statisztika" class="btn btn-primary">Vissza a statisztikákhoz</a>';
?>