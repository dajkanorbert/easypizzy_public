<h3>Törzsadatok kezelése</h3>
<hr>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fas fa-bread-slice"></i></div>
		<div class="boxcontent">
			<h3>Tészták kezelése</h3>
			<a href="?pg=tesztak">Tovább</a>
		</div>
	</div>
</div>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fas fa-cogs"></i></div>
		<div class="boxcontent">
			<h3>Szószok kezelése</h3>
			<a href="?pg=szoszok">Tovább</a>
		</div>
	</div>
</div>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fas fa-drumstick-bite"></i></div>
		<div class="boxcontent">
			<h3>Húsok kezelése</h3>
			<a href="?pg=husok">Tovább</a>
		</div>
	</div>
</div>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fas fa-pepper-hot"></i></div>
		<div class="boxcontent">
			<h3>Zöldségek kezelése</h3>
			<a href="?pg=zoldsegek">Tovább</a>
		</div>
	</div>
</div>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fas fa-cheese"></i></div>
		<div class="boxcontent">
			<h3>Sajtok kezelése</h3>
			<a href="?pg=sajtok">Tovább</a>
		</div>
	</div>
</div>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fas fa-pizza-slice"></i></div>
		<div class="boxcontent">
			<h3>Pizzák kezelése</h3>
			<a href="?pg=pizzak">Tovább</a>
		</div>
	</div>
</div>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fab fa-gulp"></i></div>
		<div class="boxcontent">
			<h3>Üdítők kezelése</h3>
			<a href="?pg=uditok">Tovább</a>
		</div>
	</div>
</div>

<div class="col-xs-12 col-sm-6 boxcontainer">
	<div class="box">
		<div class="boxicon"><i class="fas fa-ice-cream"></i></div>
		<div class="boxcontent">
			<h3>Desszertek kezelése</h3>
			<a href="?pg=desszertek">Tovább</a>
		</div>
	</div>
</div>
