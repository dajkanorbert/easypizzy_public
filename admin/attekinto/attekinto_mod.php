<h3>Pizza módosítása</h3>
<hr>
<?php
	//a módosítandó termék ID-jét lekérdezzük
	$id = $_GET['id'];

	if (isset($_POST['modosit']))
	{
		//változóba mentjük a beírt adatokat és az escapeshellcmd-vel levédjük sql injection támadás ellen
		$megnevezes = escapeshellcmd($_POST['pizzanev']);
		$meretID = $_POST['meretek'];
		$tesztaID = $_POST['tesztak'];
		$szoszID = $_POST['szoszok'];
		$husID = $_POST['husok'];
		$sajtID = $_POST['sajtok'];
		$zoldsegID = $_POST['zoldsegek'];

		$avatar=$_FILES['fileToUpload']['name'];


		//ha üres valamelyik kötelezően kitöltendő mezőnk akkor írjon ki hibaszöveget
		if (empty($megnevezes) || empty($meretID) || empty($tesztaID) || empty($szoszID) || empty($husID) || empty($sajtID) || empty($zoldsegID))
		{
			showError("Nem adtál meg minden adatot!");
		}

			//lekérdezzük megnevezés alapján és, ha 0-nál több találatot ad vissza akkor a termék már létezik az adatbázisban

			else
			{
				//ha minden adat rendben van akkor beszúrjuk a táblába a terméket és visszaírányítjuk a termékek "főoldalára"
				$db->query("UPDATE pizzak SET megnevezes='$megnevezes' , meretID=$meretID, tesztaID=$tesztaID , szoszID=$szoszID , husID=$husID , zoldsegID=$zoldsegID , sajtID=$sajtID WHERE ID = $id");

				// Képfeltöltés
			    if(!empty($avatar))
			    {
			      uploadFile($_FILES['fileToUpload'],'target:../termekkepek/avatar|maxsize:2|allow:jpg,png,bmp,jpeg|filename:'.$avatar);
			      $db->query("UPDATE pizzak SET img='$avatar' WHERE ID=$id");
			    }

				header("location: index.php?pg=pizzak");
			}
		}

	 

	$db->query("SELECT * FROM attekinto WHERE ID=$id");
	$pizzak = $db->fetchAll();
	// Űrlap új adat felvételéhez selectes kiválasztással
	echo '
	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
	<form method="POST" action="index.php?pg=attekinto_mod&id='.$id.'" enctype="multipart/form-data">
		<div class="form-group">
			<label>Megnevezés:</label>
			<input type="text" name="pizzanev" class="form-control" value="'.$pizzak[0]['megnevezes'].'">
		</div>

		<div class="form-group">
			<label>Méret:</label>';
			$db->query("SELECT * FROM meretek");
			// ez a megoldás:
			$_POST['meretek'] = $pizzak[0]['meret'];
			$db->convertSelect('ID', 'meret', '');
			echo '
		</div>

		<div class="form-group">
			<label>Tészta:</label>';
			$db->query("SELECT * FROM tesztak");
			$_POST['tesztak'] = $pizzak[0]['teszta'];
			$db->convertSelect('ID', 'megnevezes', '');
			echo '
		</div>

		<div class="form-group">
			<label>Szósz:</label>';
			$db->query("SELECT * FROM szoszok");
			$_POST['szoszok'] = $pizzak[0]['szosz'];
			$db->convertSelect('ID', 'megnevezes', '');
			echo '
		</div>


		<div class="form-group">
			<label>Hús:</label>';
			$db->query("SELECT * FROM husok");
			$_POST['husok'] = $pizzak[0]['hus'];
			$db->convertSelect('ID', 'megnevezes', '');
			echo '
		</div>


		<div class="form-group">
			<label>Sajt:</label>';
			$db->query("SELECT * FROM sajtok");
			$_POST['sajtok'] = $pizzak[0]['sajt'];
			$db->convertSelect('ID', 'megnevezes', '');
			echo '
		</div>

		<div class="form-group">
			<label>Zöldség:</label>';
			$db->query("SELECT * FROM zoldsegek");
			$_POST['zoldsegek'] = $pizzak[0]['zoldseg'];
			$db->convertSelect('ID', 'megnevezes', '');
			echo '
		</div>';

			$db->query("SELECT img FROM pizzak WHERE ID=$id");
			$pizzak = $db->fetchAll();

		if (empty($pizzak[0]['img']))
		{
		  echo '<input type="file" name="fileToUpload" id="fileToUpload"><br />';
		}
		else
		{
			print ($pizzak[0]['img']);

			echo'
		  <a href="index.php?pg=pizzak/avatardel&id='.$id.'" class="btn btn-danger">Termékkép törlése</a><br><br> ';
		}

		echo'
		<div class="form-group">
			<input type="submit" name="modosit" value="Módosít" class="btn btn-primary">
			<a href="index.php?pg=pizzak" class="btn btn-primary">Vissza a pizzák listájára</a>
		</div>
	</form>
	</div>
	<div class="col-xs-12 col-sm-3"></div>
	';
?>