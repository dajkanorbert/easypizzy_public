<?php
	if (!isset($_SESSION['vid']))
	{
		if (isset($_POST['reg']))
		{
			header("location: index.php?pg=regisztracio");
		}
		if (isset($_POST['belep']))
		{
			$email = escapeshellcmd($_POST['email']);
			$jelszo = escapeshellcmd($_POST['jelszo']);
			if (empty($email) || empty($jelszo))
			{
				showError('Hiba! Nem adtál meg minden adatot!');
			}
			else
			{
				$db->query("SELECT * FROM vasarlok WHERE email='$email'");
				$vasarlo = $db->fetchAll();
				if ($db->numRows() == 0)
				{
					showError('Hiba! Nincs ilyen e-mail cím regisztrálva!');
				}
				else
				{
					if (MD5($jelszo) != $vasarlo[0]['jelszo'])
					{
						showError('Hiba! A megadott jelszó hibás!');
					} 
					else
					{
						if ($vasarlo[0]['status'] == 0)
						{
							showError('Hiba! Tiltott felhasználó!');
						}
						else
						{
							$_SESSION['vid'] = $vasarlo[0]['ID'];
							$_SESSION['vname'] = $vasarlo[0]['nev'];
							$_SESSION['vmail'] = $vasarlo[0]['email'];
							$_SESSION['vpic'] = $vasarlo[0]['avatar'];

							$db->query("UPDATE vasarlok SET last=CURRENT_TIMESTAMP WHERE ID=".$_SESSION['vid']);
							
							header("location: index.php");
						}
					}
				}
			}
		}
	echo '
			<form method="POST" action="index.php">
				<div class="form-group">
					<input type="text" name="email" placeholder="E-mail cím" class="form-control">
				</div>
				<div class="form-group">
					<input type="password" name="jelszo" placeholder="Jelszó" class="form-control">
				</div>
				<div class="form-group">
					<input type="submit" name="belep" value="Belépés" class="btn btn-warning">
				<input type="submit" name="reg" value="Regisztráció" class="btn btn-danger">
				</div>
			</form>';
			}
	else
	{
		// ha rákattintottunk a kilépés gombra
		if (isset($_POST['kilep']))
		{
			unset($_SESSION['vid']);
			unset($_SESSION['vmail']);
			unset($_SESSION['vname']);
			unset($_SESSION['vpic']);
			// újratöltjük az oldalt
			header("location: index.php");
		}

		echo '
		<h4>Szia,</h4>
		<h4 id="white">'.$_SESSION['vname'].'</h4>';
		if ($_SESSION['vpic'] == '')
		{
			echo '<img class="img-rounded img-responsive c img img-thumbnail avatar" src="images/avatar/nopic.jpg">';
		}
		else
		{
			echo '<a href="images/avatar/'.$_SESSION['vpic'].'" data-lightbox="X"><img class="img-rounded img-responsive c img img-thumbnail avatar" src="images/avatar/'.$_SESSION['vpic'].'"></a>';
		}
		
		echo '<br><a class="white" href="index.php?pg=profilom"><span style="font-size:16px;"><i class="fas fa-user"></i>PROFILOM</span></a>';
		
		include("kosar.php");

		echo '
		<form method="POST" action="index.php">
			<div class="form-group">
				<input type="submit" name="kilep" value="Kilépés" class="btn btn-primary">
			</div>
		</form>';

	}
?>