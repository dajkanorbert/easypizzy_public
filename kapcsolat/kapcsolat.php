<h3>Kapcsolat</h3>
<hr>
<div class="col-xs-12" id="kapcsolat">
<span class="glyphicon glyphicon-envelope"></span>
<a href="mailto:info@easypizzy.hu"><span class="white">info@easypizzy.hu</span></a> <br>
<span class="glyphicon glyphicon-phone-alt"></span>
<a href="tel:+36301234567"><span class="white">+36 30 123 4567</span></a> <br>

<!-- GoogleMaps beágyazás -->
<span class="glyphicon glyphicon-map-marker"></span><br>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d50739.08883016013!2d-121.99342096745968!3d37.361608690875165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fb68ad0cfc739%3A0x7eb356b66bd4b50e!2zU3ppbMOtY2l1bS12w7ZsZ3ksIEthbGlmb3JuaWEsIEVneWVzw7xsdCDDgWxsYW1vaw!5e0!3m2!1shu!2shu!4v1583591524880!5m2!1shu!2shu" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>