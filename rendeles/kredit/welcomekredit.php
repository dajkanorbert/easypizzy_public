<h3>Welcome Kredit</h3>
<hr>
<h4>Kérjük töltse ki saját szavaival a kérdőívet!</h4>

<?php 
	$id = $_SESSION['vid'];

	if (isset($_POST['elkuld'])) 
	{
		$hallott = escapeshellcmd($_POST['hallott']);
		$etel = escapeshellcmd($_POST['etel']);
		$ital = escapeshellcmd($_POST['ital']);
		$desszert = escapeshellcmd($_POST['desszert']);
		$web = escapeshellcmd($_POST['web']);

		if (empty($hallott) || empty($etel) || empty($ital) || empty($desszert) || empty($web)) 
		{
			showError('Hiba! Nem töltöttél ki minden mezőt!');
		}
		else
		{
			$db->query("SELECT welcome FROM vasarlok WHERE ID=$id");
			$welcome = $db->fetchAll();

			if ($welcome[0]['welcome']<1) 
			{
				showError('Hiba! A Welcome kérdőívet már kitöltötted');
			}
			else
			{
				$db->query("UPDATE vasarlok SET welcome=0 , penztarca=penztarca+300 WHERE ID=$id");
				showSuccess('Sikeres kitöltés! A 300 pontot már jóvá is írtuk a krediteid között!');

				$db->query("INSERT INTO velemenyek VALUES (null, '$id', '$hallott', '$etel', '$ital', '$desszert', '$web')");
			}
		}

	}

?>


<div class="col-xs-12 col-sm-3"></div>
<form method="POST" action="index.php?pg=rendeles/kredit/welcomekredit">
	<div class="c col-xs-12 col-sm-6">
		<div class="form-group">
			<label>Hol hallott pizzériánkról?</label>
			<textarea name="hallott" class="form-control"></textarea>
		</div>

		<div class="form-group">
			<label>Milyen jellegű ételeket fogyaszt szívesen?</label>
			<textarea name="etel" class="form-control"></textarea>
		</div>

		<div class="form-group">
			<label>Milyen jellegű italokat fogyaszt szívesen étkezés közben?</label>
			<textarea name="ital" class="form-control"></textarea>
		</div>

		<div class="form-group">
			<label>Milyen jellegű desszerteket fogyaszt szívesen?</label>
			<textarea name="desszert" class="form-control"></textarea>
		</div>

		<div class="form-group">
			<label>Milyen tanácsa, észrevetele van weboldalunkkal kapcsolatban?</label>
			<textarea name="web" class="form-control"></textarea>
		</div>

		<div class="form-group">
			<input type="submit" name="elkuld" value="Elküldöm" class="btn btn-primary">
		</div>	
	</div>
	<div class="col-xs-12 col-sm-3"></div>
</form>