<h3>Új cím hozzáadása</h3>
<hr>
<?php

	$id = $_SESSION['vid'];

	if (isset($_POST['felvesz']))
	{
		$cim = escapeshellcmd($_POST['cim']);
		$megjegyzes = escapeshellcmd($_POST['megjegyzes']);
		if (empty($cim))
		{
			showError('Nem adtál meg minden adatot!');
		}
		else
		{
				$db->query("INSERT INTO cimek VALUES(null, '$id', '$cim', '$megjegyzes')");
				header('location:index.php?pg=szemelyes/cimek');
			}
		}
?>

<div class="col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
	<form method="POST" action="?pg=szemelyes/cimek_felv">
		<div class="form-group">
			<label>Cím:</label>
			<input type="text" name="cim" class="form-control">
		</div>
		<div class="form-group">
			<label>Megjegyzés: (otthoni, munkahelyi)</label>
			<input type="text" name="megjegyzes" class="form-control">
		</div>
		<div class="form-group">
			<input type="submit" name="felvesz" value="Felvesz" class="btn btn-primary">
			<a href="?pg=szemelyes/cimek" class="btn btn-primary">Vissza a címeim listájához</a>
		</div>
	</form>
</div>
<div class="col-sm-3"></div>