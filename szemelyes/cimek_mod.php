<h3>Cím módosítása</h3>
<hr>
<?php
	//a módosítandó termék ID-jét lekérdezzük
	$id = $_GET['id'];
	$vid = $_SESSION['vid'];

	//ha rákattintottunk már a módosít gombra akkor innen fut a program
	if (isset($_POST['modosit']))
	{
		//változóba mentjük a beírt adatokat és az escapeshellcmd-vel levédjük sql injection támadás ellen
		$utca = escapeshellcmd($_POST['utca']);
		$hsz = escapeshellcmd($_POST['hsz']);
		$kt = escapeshellcmd($_POST['kt']);
		$megjegyzes = escapeshellcmd($_POST['megjegyzes']);

		//ha üres valamelyik kötelezően kitöltendő mezőnk akkor írjon ki hibaszöveget
		if (empty($utca) || empty($hsz) || empty($kt))
		{
			showError('Nem adtál meg minden adatot!');
		}
		else
		{
			//lekérdezzük megnevezés alapján és, ha 0-nál több találatot ad vissza akkor a termék már létezik az adatbázisban
			$db->query("SELECT ID FROM cimek WHERE megjegyzes='$megjegyzes' AND ID<>$id");
			if ($db->numRows() != 0)
			{
				showError('Ez a cím már megtalálható az adatbázisban!');
			}
			else
			{
				//ha minden adat rendben van akkor módosítjuk a terméket és visszaírányítjuk a terméklistára
				$db->query("UPDATE cimek SET utca='$utca', hsz='$hsz', kt=$kt, megjegyzes='$megjegyzes' WHERE vasarloID=$vid");
				header("location: index.php?pg=cimek");

			}
		}
	}

	//lekérdezzük a módosítandó termék jelenlegi paramétereit és feltöltjük vele az űrlapot
	$db->query("SELECT utca,hsz,kt,megjegyzes FROM cimek WHERE ID=$id");
	$cimek = $db->fetchAll();

	echo' 
	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
		<form method="POST" action="index.php?pg=szemelyes/cimek/cimek_mod&id='.$id.'">
			<div class="form-group">
				<label>Utca: </label>
				<input type="text" name="utca" class="form-control" value="'.$cimek[0]['utca'].'">
			</div>
				<div class="form-group">
				<label>Házszám: </label>
				<input type="text" name="hsz" class="form-control" value="'.$cimek[0]['hsz'].'">
			</div>
				<div class="form-group">
				<label>Kaputelefon: </label>
				<input type="text" name="kt" class="form-control" value="'.$cimek[0]['kt'].'">
			</div>
			<div class="form-group">
				<label>Megjegyzés: </label>
				<input type="text" name="megjegyzes" class="form-control" value="'.$cimek[0]['megjegyzes'].'">
			</div>
			<div class="form-group">
				<input type="submit" name="modosit" value="Módosít" class="btn btn-primary">
				<a href="?pg=cimek" class="btn btn-primary">Vissza a címeim listájához</a>
				<br><br>
			</div>
		</form>
	</div>
	<div class="col-xs-12 col-sm-3"></div>
	';
?>