<h3>Személyes adatok módosítása</h3>
<hr>
<?php
	$id = $_SESSION['vid'];

	if (isset($_POST['modosit']))
	{
		$nev = escapeshellcmd($_POST['nev']);
		$email = escapeshellcmd($_POST['email']);
		$tel = escapeshellcmd($_POST['tel']);

		$avatar=$_FILES['fileToUpload']['name'];

		if (empty($nev) || empty($email) || empty($tel) )
		{
			showError('Nem adtál meg minden adatot!');
		}
		else
		{
			$db->query("SELECT email FROM vasarlok WHERE email='$email' AND ID<>$id");
			if ($db->numRows() != 0)
			{
				showError('Ez az e-mail cím már foglalt!');
			}
			else
			{
				$db->query("UPDATE vasarlok SET nev='$nev', email='$email', tel='$tel' WHERE ID=$id");

			    // Képfeltöltés
			    if(!empty($avatar))
			    {
			      uploadFile($_FILES['fileToUpload'],'target:images/avatar|maxsize:2|allow:jpg,png,bmp,jpeg|filename:'.$avatar);
			      $db->query("UPDATE vasarlok SET avatar='$avatar' WHERE ID=$id");
			      $_SESSION['vpic'] = $avatar;
			    }

				header("location: index.php?pg=profilom");				
			}

		}
	}
	
	$db->query("SELECT * FROM vasarlok WHERE ID=$id");
	$vasarlok = $db->fetchAll();

	echo '
	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
	<form method="POST" action="index.php?pg=szemelyes/adataim" enctype="multipart/form-data">

		<div class="form-group">
			<label>Név</label>
			<input type="text" name="nev" class="form-control" value="'.$vasarlok[0]['nev'].'">
		</div>
			<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="form-control" value="'.$vasarlok[0]['email'].'">
		</div>
		<div class="form-group">
			<label>Telefonszám</label>
			<input type="text" name="tel" class="form-control" value="'.$vasarlok[0]['tel'].'">
		</div>
		<div class="form-group">
			<label>Profilkép</label><br>';

		if (empty($vasarlok[0]['avatar']))
		{
		  echo '<input type="file" name="fileToUpload" id="fileToUpload"><br />';
		}
		else
		{
		  echo'<img src="images/avatar/'.$vasarlok[0]['avatar'].'" class="img img-thumbnail"><br>
		  <a href="index.php?pg=szemelyes/avatardel" class="btn btn-danger">Profilkép törlése</a><br><br> ';
		}

		echo '
		</div>
		<div class="form-group">
			<input type="submit" name="modosit" class="btn btn-primary" value="Módosítás">
		</div>		
	</form>
	</div>
	<div class="col-xs-12 col-sm-3"></div>
	';
?>