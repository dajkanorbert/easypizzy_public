<h3>Cím módosítása</h3>
<hr>
<?php
	//a módosítandó termék ID-jét lekérdezzük
	$id = $_GET['id'];
	$vid = $_SESSION['vid'];

	//ha rákattintottunk már a módosít gombra akkor innen fut a program
	if (isset($_POST['modosit']))
	{
		//változóba mentjük a beírt adatokat és az escapeshellcmd-vel levédjük sql injection támadás ellen
		$cim = escapeshellcmd($_POST['cim']);
		$megjegyzes = escapeshellcmd($_POST['megjegyzes']);

		//ha üres valamelyik kötelezően kitöltendő mezőnk akkor írjon ki hibaszöveget
		if (empty($cim))
		{
			showError('Nem adtál meg minden adatot!');
		}
		
			else
			{
				//ha minden adat rendben van akkor módosítjuk a terméket és visszaírányítjuk a terméklistára
				$db->query("UPDATE cimek SET cim='$cim', megjegyzes='$megjegyzes' WHERE vasarloID=$vid AND ID=$id");
				header("location: index.php?pg=szemelyes/cimek");

			}
		}
	

	//lekérdezzük a módosítandó termék jelenlegi paramétereit és feltöltjük vele az űrlapot
	$db->query("SELECT cim,megjegyzes FROM cimek WHERE ID=$id");
	$cimek = $db->fetchAll();

	echo' 
	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
		<form method="POST" action="index.php?pg=cimek_mod&id='.$id.'">
			<div class="form-group">
				<label>Cím: </label>
				<input type="text" name="cim" class="form-control" value="'.$cimek[0]['cim'].'">
			<div class="form-group">
				<label>Megjegyzés: </label>
				<input type="text" name="megjegyzes" class="form-control" value="'.$cimek[0]['megjegyzes'].'">
			</div>
			<div class="form-group">
				<input type="submit" name="modosit" value="Módosít" class="btn btn-primary">
				<a href="index.php?pg=szemelyes/cimek" class="btn btn-primary">Vissza a címeim listájához</a>
				<br><br>
			</div>
		</form>
	</div>
	<div class="col-xs-12 col-sm-3"></div>
	';
?>