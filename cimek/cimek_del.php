<h3>Cím törlése</h3>
<hr>
<?php

	//a törlendő termék ID-jét lekérdezzük
	$id = $_GET['id'];

	//ha rákattintott a töröl gombra
	if (isset($_POST['torol']))
	{
		//kitöröljük az adatbázisból és visszairányítjuk a desszertekhez
		$db->query("DELETE FROM cimek WHERE ID=$id");
		header("location: index.php?pg=szemelyes/cimek");
	}

	// ide fut először, hiszen az oldal betöltése után nem kattintottunk egyből a töröl gombra, biztonsági kérdés a termék törléséről
	echo 'Biztosan törlöd a következő címet?';

	$db->query("SELECT ID AS 'Azonosító', cim AS 'Cím' , megjegyzes AS 'Megjegyzés' FROM cimek WHERE ID=$id");

	$db->showRekord();

	echo '<form method="POST" action="index.php?pg=cimek_del&id='.$id.'">
		<div class="form-group">
			<input type="submit" name="torol" value="Törlés" class="btn btn-danger">
			<a href="?pg=szemelyes/cimek" class="btn btn-primary">Mégsem</a>
		</div>
	</form>';
?>
