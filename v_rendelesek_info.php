<h3>Rendelés értékelő</h3>
<hr>
<?php

	$id=$_GET['id'];
	$vid = $_SESSION['vid'];

	if (isset($_POST['felvesz'])) {
		$db->query("UPDATE rendelestetel SET ertekelve=1 WHERE ID=$id");

		// szakács értékelő
		$etel = $_POST['etel'];
		$db->query("SELECT szakacs_azonosito FROM v_rendelesek WHERE ID = $id");
		$res=$db->fetchAll();
		$szakacs = $res[0]['szakacs_azonosito'];
		$db->query("UPDATE felhasznalok SET pontszam=pontszam+$etel WHERE ID=$szakacs");


		//szállítás értékelés
		$szallitas = $_POST['szallitas'];
		$db->query("SELECT futar_azonosito FROM v_rendelesek WHERE id=$id");
		$ress=$db->fetchAll();
		$futar = $ress[0]['futar_azonosito'];
		$db->query("UPDATE felhasznalok SET pontszam=pontszam+$szallitas WHERE ID=$futar");

		//pénztárcába 200 pont jóváírás
		$db->query("UPDATE vasarlok SET penztarca=penztarca+200 WHERE ID=$vid");

		header('location:index.php?pg=szemelyes/ertekeles');
	}

	$db->query("SELECT rendelesid AS 'Rendelés azonosító' , vasarlo AS 'Vásárló' , pizza AS 'Pizza' , udito AS 'Üdítő' , desszert AS 'Desszert' , mennyiseg AS 'Mennyiség' , vegosszeg AS 'Végösszeg' , fizmod AS 'Fizetési mód' , kiszallitasi_mod AS 'Kiszállítási mód' , kiszallitasi_cim AS 'Kiszállítási cím' , megjegyzes AS 'Megjegyzés' , datum AS 'Dátum' , szakacs_azonosito AS 'Szakács azonosító' , futar_azonosito AS 'Futár azonosító' FROM v_rendelesek WHERE ID=$id");
	$db->showRekord();

	echo'

	<form method="POST" action="index.php?pg=v_rendelesek_info&id='.$id.'">
	<h4>Mennyire volt elégedett az étel minőségével?</h4>
	<select id="etel" name="etel">
	<option value="">Válasszon!</option>
	<option value="100">Nagyon elégedett voltam!</option>
	<option value="50">Találtam kivetni valót benne!</option>
	<option value="0">Abszolút elégedetlen voltam!</option>
	</select>

	<h4>Mennyire volt megelégedve a házhozszállítás minőségével?</h4>
	<select id="szallitas" name="szallitas">
	<option value="">Válasszon!</option>
	<option value="100">Nagyon elégedett voltam!</option>
	<option value="50">Találtam kivetni valót benne!</option>
	<option value="0">Abszolút elégedetlen voltam!</option>
	</select>

	<div class="col-xs-12">

	<input type="submit" name="felvesz" value="Értékelés elküldése" class="btn btn-danger">
	</div>

	<div class="col-xs-12">
	<a href="?pg=szemelyes/ertekeles" class="btn btn-primary">Vissza az értékelések listájára</a>
	</div>';

?>