<script>
  jQuery(document).ready(function() {
    jQuery('ul.sf-menu').superfish();
  });
</script>

	<ul class="sf-menu">
				<li class="current">
					<a href="index.php">Főoldal</a>
				</li>


				<li class="current">
					<a href="#">Rendelés</a>
					<ul>
						<li>
							<a href="index.php?pg=rendeles/pizzak">Pizza</a>
							<ul>
								<li><a href="index.php?pg=rendeles/yourpizza">Saját ízlésre szabva</a></li>
								<li><a href="index.php?pg=rendeles/nepszeru">Népszerű ízek</a></li>
							</ul>
						</li>
						<li class="current">
							<a href="index.php?pg=rendeles/desszertek">Desszert</a>
						</li>
						<li>
							<a href="index.php?pg=rendeles/uditok">Üdítő</a>
						</li>
					</ul>
				</li>

				<li class="current">
					<a href="#">Akciók</a>
					<ul>
						<li>
							<a href="index.php?pg=rendeles/5kedvezmeny">5% kedvezmény!</a>
						</li>
						<li>
							<a href="index.php?pg=rendeles/haromhelyettnegy">Három helyett négyet adunk</a>
						</li>
					</ul>
				</li>


				<li>
					<a href="followed.html">menu item 2</a>
				</li>
				<li>
					<a href="followed.html">menu item 3</a>
					<ul>
						<li>
							<a href="followed.html">menu item</a>
							<ul>
								<li><a href="followed.html">short</a></li>
								<li><a href="followed.html">short</a></li>
								<li><a href="followed.html">short</a></li>
								<li><a href="followed.html">short</a></li>
								<li><a href="followed.html">short</a></li>
							</ul>
						</li>
						<li>
							<a href="followed.html">menu item</a>
							<ul>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
							</ul>
						</li>
						<li>
							<a href="followed.html">menu item</a>
							<ul>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
							</ul>
						</li>
						<li>
							<a href="followed.html">menu item</a>
							<ul>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
							</ul>
						</li>
						<li>
							<a href="followed.html">menu item</a>
							<ul>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
								<li><a href="followed.html">menu item</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li>
					<a href="followed.html">menu item 4</a>
				</li>	
			</ul>